HTMLSTYLE overlib
HEIGHT 600
WIDTH 600
HTMLOUTPUTFILE index.html
IMAGEOUTPUTFILE weathermap.png
FONTDEFINE 41 /var/www/wmap/fonts/VerilySerifMono.ttf 6
FONTDEFINE 42 /var/www/wmap/fonts/VerilySerifMono.ttf 5
FONTDEFINE 43 /var/www/wmap/fonts/Quicksand.ttf 7

SCALE  0   5 140  32 255 <5%
SCALE  5  10 140   0 255 <10%
SCALE 10  15  32  64 255 <15%
SCALE 15  20  32 128 255 <20%
SCALE 20  25   0 168 255 <25%
SCALE 25  30   0 192 255 <30%
SCALE 30  35   0 204 192 <35%
SCALE 35  40   0 216 128 <40%
SCALE 40  45   0 228  64 <45%
SCALE 45  50   0 240   0 <50%
SCALE 50  55  80 240   0 <55%
SCALE 55  60 112 240   0 <60%
SCALE 60  65 144 240   0 <65%
SCALE 65  70 176 240   0 <70%
SCALE 70  75 208 240   0 <75%
SCALE 75  80 240 240   0 <80%
SCALE 80  85 255 224   0 <85%
SCALE 85  90 255 192   0 <90%
SCALE 90  95 255 160   0 <95%
SCALE 95 100 255   0   0  mucho
KEYPOS DEFAULT 155 515
KEYSTYLE DEFAULT horizontal

NODE adsl
    POSITION 100 40
    LABEL Free
    LABELFONT 43
    ICON 80 80 /var/www/wmap/images/cloud.png

NODE sdsl
    POSITION 500 40
    LABEL Ovh
    LABELFONT 43
    ICON 80 80 /var/www/wmap/images/cloud.png

NODE fw1
    POSITION 140 140
    LABEL Dysnomia
    LABELFONT 43
    LABELOFFSET E
    LABELOUTLINECOLOR 255 255 255
    ICON 60 60 /var/www/wmap/images/fw.png

NODE fw2
    POSITION 235 140
    LABEL Androktasiai
    LABELFONT 43
    LABELOFFSET E
    LABELOUTLINECOLOR 255 255 255
    ICON 60 60 /var/www/wmap/images/fw.png

NODE fw3
    POSITION 355 140
    LABEL Phonoi
    LABELFONT 43
    LABELOFFSET E
    LABELOUTLINECOLOR 255 255 255
    ICON 60 60 /var/www/wmap/images/fw.png

NODE fw4
    POSITION 460 140
    LABEL Makhai
    LABELFONT 43
    LABELOFFSET E
    LABELOUTLINECOLOR 255 255 255
    ICON 60 60 /var/www/wmap/images/fw.png

NODE sw1
    POSITION 200 240
    LABEL Ponos
    LABELFONT 43
    LABELOFFSET W
    LABELOUTLINECOLOR 255 255 255
    ICON 60 60 /var/www/wmap/images/sw.png

NODE sw2
    POSITION 400 240
    LABEL Ate
    LABELFONT 43
    LABELOFFSET E
    LABELOUTLINECOLOR 255 255 255
    ICON 60 60 /var/www/wmap/images/sw.png

NODE sw3
    POSITION 490 450
    LABEL Lethe
    LABELFONT 43
    LABELOFFSET S
    LABELOUTLINECOLOR 255 255 255
    ICON 60 60 /var/www/wmap/images/hub.png

NODE sw4
    POSITION 110 450
    LABEL Algea
    LABELFONT 43
    LABELOFFSET S
    LABELOUTLINECOLOR 255 255 255
    ICON 60 60 /var/www/wmap/images/hub.png

NODE vs1
    POSITION 300 380
    LABEL Nemesis
    LABELFONT 43
    LABELOFFSET S
    LABELOUTLINECOLOR 255 255 255
    ICON 60 60 /var/www/wmap/images/server.png

NODE vs2
    POSITION 400 375
    LABEL Thanatos
    LABELFONT 43
    LABELOFFSET S
    LABELOUTLINECOLOR 255 255 255
    ICON 60 60 /var/www/wmap/images/server.png

NODE vs3
    POSITION 500 360
    LABEL Oneiroi
    LABELFONT 43
    LABELOFFSET S
    LABELOUTLINECOLOR 255 255 255
    ICON 60 60 /var/www/wmap/images/server.png

NODE store1
    POSITION 100 360
    LABEL Phosphorus
    LABELFONT 43
    LABELOFFSET W
    LABELOUTLINECOLOR 255 255 255
    ICON 60 60 /var/www/wmap/images/disk.png

NODE store2
    POSITION 200 375
    LABEL Hesperus
    LABELFONT 43
    LABELOFFSET S
    LABELOUTLINECOLOR 255 255 255
    ICON 60 60 /var/www/wmap/images/disk.png

NODE ap1
    POSITION 240 450
    LABEL Asteros
    LABELFONT 43
    LABELOFFSET S
    LABELOUTLINECOLOR 255 255 255
    ICON 60 60 /var/www/wmap/images/ap.png

NODE guestwifi
    POSITION 380 425
    LABEL Guests VLAN
    LABELFONT 43
    LABELOFFSET E
    LABELOUTLINECOLOR 255 255 255
    ICON 30 30 /var/www/wmap/images/cloud.png

NODE userswifi
    POSITION 390 450
    LABEL WiFi VLAN
    LABELFONT 43
    LABELOFFSET E
    LABELOUTLINECOLOR 255 255 255
    ICON 30 30 /var/www/wmap/images/cloud.png

NODE faustwifi
    POSITION 380 475
    LABEL Faust VLAN
    LABELFONT 43
    LABELOFFSET E
    LABELOUTLINECOLOR 255 255 255
    ICON 30 30 /var/www/wmap/images/cloud.png

LINK fw1sdsl
    NODES fw1:NE85 sdsl
    TARGET /var/www/mrtg/dysnomia/10.42.242.2_10.rrd:ds0:ds1
    BANDWIDTH 3024K 2320K
    BWFONT 41
    BWLABELPOS 70 50
    ARROWSTYLE 1 1
    WIDTH 3

LINK fw2sdsl
    NODES fw2:NE85 sdsl
    TARGET /var/www/mrtg/androktasiai/10.42.242.3_10.rrd:ds0:ds1
    BANDWIDTH 3024K 2320K
    BWFONT 41
    BWLABELPOS 67 47
    ARROWSTYLE 1 1
    WIDTH 3

LINK fw3sdsl
    NODES fw3:N80 sdsl
    TARGET /var/www/mrtg/phonoi/10.42.242.4_10.rrd:ds0:ds1
    BANDWIDTH 3024K 2320K
    BWFONT 41
    BWLABELPOS 64 44
    ARROWSTYLE 1 1
    WIDTH 3

LINK fw4sdsl
    NODES fw4:N80 sdsl
    TARGET /var/www/mrtg/makhai/10.42.242.5_10.rrd:ds0:ds1
    BANDWIDTH 3024K 2320K
    BWFONT 41
    BWLABELPOS 60 30
    ARROWSTYLE 1 1
    WIDTH 3

LINK fw1adsl
    NODES fw1:NE85 adsl
    TARGET /var/www/mrtg/dysnomia/10.42.242.2_9.rrd:ds0:ds1
    BANDWIDTH 7049K 1066K
    BWFONT 41
    BWLABELPOS 60 30
    ARROWSTYLE 1 1
    WIDTH 3

LINK fw2adsl
    NODES fw2:NE85 adsl
    TARGET /var/www/mrtg/androktasiai/10.42.242.3_9.rrd:ds0:ds1
    BANDWIDTH 7049K 1066K
    BWFONT 41
    BWLABELPOS 64 44
    ARROWSTYLE 1 1
    WIDTH 3

LINK fw3adsl
    NODES fw3:NW71 adsl
    TARGET /var/www/mrtg/phonoi/10.42.242.4_9.rrd:ds0:ds1
    BANDWIDTH 7049K 1066K
    BWFONT 41
    BWLABELPOS 67 47
    ARROWSTYLE 1 1
    WIDTH 3

LINK fw4adsl
    NODES fw4:NW70 adsl
    TARGET /var/www/mrtg/makhai/10.42.242.5_9.rrd:ds0:ds1
    BANDWIDTH 7049K 1066K
    BWFONT 41
    BWLABELPOS 70 50
    ARROWSTYLE 1 1
    WIDTH 3

LINK fw1sync
    NODES fw1:SE55 sw1:N56
    TARGET /var/www/mrtg/dysnomia/10.42.242.2_3.rrd:ds0:ds1
    BANDWIDTH 100M
    BWLABEL none
    ARROWSTYLE 1 1
    WIDTH 1

LINK fw2sync
    NODES fw2:S sw1:NE50
    TARGET /var/www/mrtg/androktasiai/10.42.242.3_3.rrd:ds0:ds1
    BANDWIDTH 100M
    BWLABEL none
    ARROWSTYLE 1 1
    WIDTH 1

LINK fw3sync
    NODES fw3:SE70 sw2:N52
    TARGET /var/www/mrtg/phonoi/10.42.242.4_3.rrd:ds0:ds1
    BANDWIDTH 100M
    BWLABEL none
    ARROWSTYLE 1 1
    WIDTH 1

LINK fw4sync
    NODES fw4:S90 sw2:NE70
    TARGET /var/www/mrtg/makhai/10.42.242.5_3.rrd:ds0:ds1
    BANDWIDTH 100M
    BWLABEL none
    ARROWSTYLE 1 1
    WIDTH 1

LINK fw1lan
    NODES fw1:S90 sw1:NW60
    TARGET /var/www/mrtg/dysnomia/10.42.242.2_2.rrd:ds0:ds1
    BANDWIDTH 100M
    BWFONT 41
    BWLABELPOS 40 20
    ARROWSTYLE 1 1
    WIDTH 2

LINK fw2lan
    NODES fw2:SW sw1:N30
    TARGET /var/www/mrtg/androktasiai/10.42.242.3_2.rrd:ds0:ds1
    BANDWIDTH 100M
    BWFONT 41
    BWLABELPOS 30 10
    ARROWSTYLE 1 1
    WIDTH 2

LINK fw3lan
    NODES fw3:S90 sw2:NW45
    TARGET /var/www/mrtg/phonoi/10.42.242.4_2.rrd:ds0:ds1
    BANDWIDTH 100M
    BWFONT 41
    BWLABELPOS 30 10
    ARROWSTYLE 1 1
    WIDTH 2

LINK fw4lan
    NODES fw4:SW85 sw2:N40
    TARGET /var/www/mrtg/makhai/10.42.242.5_2.rrd:ds0:ds1
    BANDWIDTH 100M
    BWFONT 41
    BWLABELPOS 40 20
    ARROWSTYLE 1 1
    WIDTH 2

LINK tosw3
    NODES sw2:SE10 sw3:N90
    TARGET /var/www/mrtg/ate/10.42.242.9_24.rrd:ds0:ds1
    BANDWIDTH 1000M
    BWFONT 41
    BWLABELPOS 70 50
    ARROWSTYLE 1 1
    WIDTH 2

LINK tosw4
    NODES sw1:SW10 sw4:N90
    TARGET /var/www/mrtg/ponos/10.42.242.6_24.rrd:ds0:ds1
    BANDWIDTH 1000M
    BWFONT 41
    BWLABELPOS 70 50
    ARROWSTYLE 1 1
    WIDTH 2

LINK swlagg1
    NODES sw1:NE50 sw2:NW50
    TARGET /var/www/mrtg/ponos/10.42.242.6_21.rrd:ds0:ds1
    BANDWIDTH 1000M
    BWFONT 41
    ARROWSTYLE 1 1
    WIDTH 2

LINK swlagg2
    NODES sw1:SE50 sw2:SW50
    TARGET /var/www/mrtg/ponos/10.42.242.6_22.rrd:ds0:ds1
    BANDWIDTH 1000M
    BWFONT 41
    ARROWSTYLE 1 1
    WIDTH 2

LINK store1lagg1
    NODES sw1:W80 store1:W60
    TARGET /var/www/mrtg/ponos/10.42.242.6_11.rrd:ds0:ds1
    BANDWIDTH 1000M
    BWFONT 42
    BWLABELPOS 70 20
    ARROWSTYLE 1 1
    WIDTH 1

LINK store1lagg2
    NODES sw1:W57 store1:W20
    TARGET /var/www/mrtg/ponos/10.42.242.6_12.rrd:ds0:ds1
    BANDWIDTH 1000M
    BWFONT 42
    BWLABELPOS 55 40
    ARROWSTYLE 1 1
    WIDTH 1

LINK store1lagg3
    NODES sw1:W35 store1:E20
    TARGET /var/www/mrtg/ponos/10.42.242.6_13.rrd:ds0:ds1
    BANDWIDTH 1000M
    BWFONT 42
    BWLABELPOS 65 30
    ARROWSTYLE 1 1
    WIDTH 1

LINK store1lagg4
    NODES sw1:W13 store1:E60
    TARGET /var/www/mrtg/ponos/10.42.242.6_14.rrd:ds0:ds1
    BANDWIDTH 1000M
    BWFONT 42
    BWLABELPOS 55 44
    ARROWSTYLE 1 1
    WIDTH 1

LINK store2lagg1
    NODES sw1:W15 store2:W20
    TARGET /var/www/mrtg/ponos/10.42.242.6_15.rrd:ds0:ds1
    BANDWIDTH 1000M
    BWFONT 42
    BWLABELPOS 65 34
    ARROWSTYLE 1 1
    WIDTH 1

LINK store2lagg2
    NODES sw1:E15 store2:E20
    TARGET /var/www/mrtg/ponos/10.42.242.6_16.rrd:ds0:ds1
    BANDWIDTH 1000M
    BWFONT 42
    BWLABELPOS 55 44
    ARROWSTYLE 1 1
    WIDTH 1

LINK vs1lan
    NODES sw2:SW75 vs1:NE85
    TARGET /var/www/mrtg/ate/10.42.242.9_19.rrd:ds0:ds1
    BANDWIDTH 1000M
    BWFONT 42
    ARROWSTYLE 1 1
    WIDTH 2

LINK vs2lan
    NODES sw2:S vs2:N
    TARGET /var/www/mrtg/ate/10.42.242.9_18.rrd:ds0:ds1
    BANDWIDTH 1000M
    BWFONT 42
    BWLABELPOS 80 20
    ARROWSTYLE 1 1
    WIDTH 2

LINK vs3lan
    NODES sw2:SE75 vs3:NW75
    TARGET /var/www/mrtg/ate/10.42.242.9_17.rrd:ds0:ds1
    BANDWIDTH 1000M
    BWFONT 42
    ARROWSTYLE 1 1
    WIDTH 2

LINK wifi
    NODES ap1:W90 sw4:E90
    TARGET /var/www/mrtg/asteros/10.42.242.11_2.rrd:ds0:ds1
    BANDWIDTH 1000M
    BWFONT 42
    ARROWSTYLE 1 1
    WIDTH 2

LINK ssidguest
    NODES ap1:E90 guestwifi
    TARGET /var/www/mrtg/asteros/10.42.242.11_3.rrd:ds0:ds1
    BANDWIDTH 56M
    BWFONT 42
    BWLABELPOS 70 32
    ARROWSTYLE 1 1
    WIDTH 2

LINK ssidusers
    NODES ap1:E90 userswifi
    TARGET /var/www/mrtg/asteros/10.42.242.11_14.rrd:ds0:ds1
    BANDWIDTH 56M
    BWFONT 42
    BWLABELPOS 75 50
    ARROWSTYLE 1 1
    WIDTH 2

LINK ssidfaust
    NODES ap1:E90 faustwifi
    TARGET /var/www/mrtg/asteros/10.42.242.11_13.rrd:ds0:ds1
    BANDWIDTH 56M
    BWFONT 42
    BWLABELPOS 60 18
    ARROWSTYLE 1 1
    WIDTH 2
