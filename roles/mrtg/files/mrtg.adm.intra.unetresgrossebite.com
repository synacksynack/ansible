var aps = { 'asteros': { 'alias': 'ap1', 'ip': '10.42.242.11', 'ifs': { 'Ethernet': '2', 'Guest': '3', 'Faust': '13', 'Users': '14' } } };
var firewalls = { 'dysnomia': { 'alias': 'fw1', 'ip': '10.42.242.2', 'ifs': { 'WAN-global': '1', 'WAN-Free': '9', 'WAN-OVH': '10', 'SYNC': '3', 'VLANs': '2', 'Admin': '11', 'SIP': '18', 'VMs': '13', 'WiFi': '16', 'Faust': '14', 'Plaristote': '17', 'Friends': '15', 'Users': '12', 'Guests': '8'} }, 'androktasiai': { 'alias': 'fw2', 'ip': '10.42.242.3', 'ifs': { 'WAN-global': '1', 'WAN-Free': '9', 'WAN-OVH': '10', 'SYNC': '3', 'VLANs': '2', 'Admin': '11', 'SIP': '18', 'VMs': '13', 'WiFi': '16', 'Faust': '14', 'Plaristote': '17', 'Friends': '15', 'Users': '12', 'Guests': '8' } }, 'phonoi': { 'alias': 'fw3', 'ip': '10.42.242.4', 'ifs': { 'WAN-global': '1', 'WAN-Free': '9', 'WAN-OVH': '10', 'SYNC': '3', 'VLANs': '2', 'Admin': '11', 'SIP': '18', 'VMs': '13', 'WiFi': '16', 'Faust': '14', 'Plaristote': '17', 'Friends': '15', 'Users': '12', 'Guests': '8'} }, 'makhai': { 'alias': 'fw4', 'ip': '10.42.242.5', 'ifs': { 'WAN-global': '1', 'WAN-Free': '9', 'WAN-OVH': '10', 'SYNC': '3', 'VLANs': '2', 'Admin': '11', 'SIP': '18', 'VMs': '13', 'WiFi': '16', 'Faust': '14', 'Plaristote': '17', 'Friends': '15', 'Users': '12', 'Guests': '8' } } };
var sans = { 'hesperus': { 'alias': 'san1', 'ip': '10.42.242.12', 'ifs': { 'bge0': '1', 'bge1': '2', 'Admin': '5', 'Plaristote': '7', 'Faust': '6', 'HAST': '8', 'lagg0': '4' } }, 'phosphorus': { 'alias': 'san2', 'ip': '10.42.242.13', 'ifs': { 'igb0': '1', 'igb1': '2', 'em0': '3', 'em1': '4', 'Admin': '7', 'Plaristote': '9', 'Faust': '8', 'HAST': '10', 'lagg0': '6' } } };
var switches = { 'ponos': { 'alias': 'sw1', 'ip': '10.42.242.6', 'ifs': { 'WAN@fw1 (g1)': '1', 'VLANs@fw1 (g2)': '2', 'WAN@fw2 (g3)': '3', 'VLANs@fw2 (g4)': '4', 'SYNC@fw1 (g5)': '5', 'SYNC@fw2 (g6)': '6', 'em0@san2 (g11)': '11', 'em1@san2 (g12)': '12', 'igb0@san2 (g13)': '13', 'igb1@san2 (g14)': '14', 'bge0@san1 (g15)': '15', 'bge1@san1 (g16)': '16', 'LAGG#1 (g21)': '21', 'LAGG#2 (g22)': '22', 'Algea (g24)': '24', 'LAGG#tot': '49', 'san2#tot': '50', 'san1#tot': '51' } }, 'ate': { 'alias': 'sw2', 'ip': '10.42.242.9', 'ifs': { 'WAN@fw3 (g1)': '1', 'VLANs@fw3 (g2)': '2', 'WAN@fw4 (g3)': '3', 'VLANs@fw4 (g4)': '4', 'SYNC@fw3 (g5)': '5', 'SYNC@fw4 (g6)': '6', 'Oneiroi (g17)': '17', 'Thanatos (g18)': '18', 'Nemesis (g19)' :'19', 'LAGG#1 (g21)': '21', 'LAGG#2 (g22)': '22', 'Lethe (g24)': '24', 'LAGG#tot': '25' } } };
