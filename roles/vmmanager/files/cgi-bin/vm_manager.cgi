#!/usr/bin/perl -w

use strict;
use File::Glob ':glob';
use CGI(\*STDIN);
use Script::Config;
use Script::Tools;
use VM::VM;

my $cgi = CGI->new();
my $START_VM = "start";
my $STOP_VM = "stop";
my $START_DAY = "0";
my $START_WEEK = "1";
my $DIRNAME="..";
my $HIDE_VM_LIST = "$DIRNAME/scripts/vm_hide_list.txt";
my $server_name = $cgi->server_name();

main();

sub display_vm_list
{
    my ( $srv ) = @_;
    my $host = $srv->{host};
    my ( $vm, $item );
    my $i = 0;
    my @vm_list = sort( $srv->get_all_vm() );
    my @info = $srv->get_header_stats();
    my $hide_vm;
    my $count = 0;

    print $cgi->hr;
    print "
    <style type=\"text/css\">
	tr.row_odd { background: #fefefe; }
	tr.row_even { background: #efefef; }
	tr.row_over { background: #ffffce; }
    </style>

    <table width=\"98%\" cellspacing=\"0\" cellpadding=\"3\">
	<tr>
	    <th style=\"text-align: center\">VM</th>\n
	    <th style=\"text-align: center\" colspan=2 width=20%>Action</th>\n
	    <th style=\"text-align: center\">State</th>\n";

    foreach ( @info )
    {
	print "	    <th style=\"text-align: right\">$_</th>\n" if ( ++$i < scalar(@info) );
    }
    print "	</tr>\n";

    foreach $vm ( @vm_list )
    {
	my $class = ($count % 2) ? "even" : "odd";

	$hide_vm = `grep -q ^$vm\$ $HIDE_VM_LIST 2>/dev/null && echo 1 || echo 0`;

	print "	<tr onmouseover=\"this.className='row_over';\" onmouseOut=\"this.className='row_$class';\" class=\"row_$class\">\n";
	print "		    <td><a name='$vm' style=\"text-decoration: none\"/>$vm</td>\n";

	if ( $srv->get_vm_state( $vm ) == 1 )
	{
	    @info = $srv->get_vm_stats( $vm );

	    if ( $hide_vm == 1 )
	    {
		print "	    <td colspan=2> </td>\n";
	    }
	    else
	    {
		print "	    <td style=\"text-align: center\" colspan=2>".$cgi->a({-href=>"javascript:OpenPopup('popup.cgi?host=$host&vm=$vm&action=$STOP_VM', '', '$POPUP_WIDTH', '$POPUP_HEIGHT', '$vm' )", -title=>"Stop $vm" },"<img src=\"icons/stop.png\" alt=\"Stop $vm\">")."</td>\n";
	    }
	    print "	    <td style=\"text-align: center\"><b><font color='green'>Started</font></td>";

	    $i = 0;
	    foreach $item ( @info )
	    {
		print "	    <td style=\"text-align: right\">$item</td>\n" if ( ++$i < scalar(@info) );
	    }
	}
	else
	{
	    if ( $hide_vm == 1 )
	    {
		print "	    <td></td>\n";
		print "	    <td></td>\n";
	    }
	    else
	    {
		print "	    <td style=\"text-align: center\">".$cgi->a({-href=>"vm_manager_action.cgi?host=$host&vm=$vm&action=$START_VM&week=$START_DAY", -title=>"Start $vm for the day" },"<img src=\"icons/start-day.png\" alt=\"Start $vm for the day\">")."</td>\n";
		print "	    <td style=\"text-align: center\">".$cgi->a({-href=>"vm_manager_action.cgi?host=$host&vm=$vm&action=$START_VM&week=$START_WEEK", -title=>"Start $vm for the week" },"<img src=\"icons/start-week.png\" alt=\"Start $vm for the week\">")."</td>\n";
	    }
	    print "	    <td style=\"text-align: center\"><b><font color='red'>Stopped</font></b></td>\n";

	    $i = 0;
	    foreach $item ( @info )
	    {
		print "	    <td style=\"text-align: right\"><b>-</b></td>\n" if ( ++$i < scalar(@info) );
	    }
	}
	print "	</tr>\n";

	$hide_vm = 0;
	$count++;
    }

    print "    </table>\n";
    print $cgi->hr;
    print "<img src=\"icons/stop.png\">Stop<br>";
    print "<img src=\"icons/start-day.png\">Start for the day<br>";
    print "<img src=\"icons/start-week.png\">Start for the week<br>";
}

sub main
{
    my $srv;

    print $cgi->header( -charset=>'utf-8' );
    print $cgi->start_html( -title=>"$server_name: VE Management", -style=>{-src => "css/global.css"} );

    my $host;
    if ( $cgi->param('host') and $cgi->param('host') =~ /^\w+$/ )
    {
	$host = $cgi->param('host');
    }
    else
    {
	$host = "";
    }
    $srv = VM->NEW($host);

    if ( $srv )
    {
	my $display_name;

	if ( $host )
	{
	    $display_name = $host;
	}
	else
	{
	    $display_name = $server_name;
	}

	print $cgi->h2( $cgi->a( {-rel=>"Twiki", -href=>"$VMLIST_URL"}, "Server: $display_name (".$srv->get_virt_sys().")") );

	display_vm_list( $srv );

	print "<script language=\"javascript\">
    function OpenPopup(page, nom, m_width, m_height)
    {
	m_top = Math.round((screen.availHeight - m_height)/2);
	m_left = Math.round((screen.availWidth - m_width)/2);

	open( page, nom, \"top=\"+m_top+\", left=\"+m_left+\", height=\"+m_height+\", width=\"+m_width );
    }
</script>";
    }
    else
    {
	print STDERR "Unable to establish which virtualization solution is installed";
    }

    print $cgi->end_html;
}
