#
# Interface VM
#
package VM;

use strict;
use VM::OpenVZ;
use VM::Vserver;
use VM::Xen;
use VM::VMWare;
use VM::KVM;
use VM::Libvirt;

my $use_libvirt;

BEGIN
{
    $use_libvirt = eval
    { 
	require Sys::Virt; 
	require VM::Libvirt; 
    };
}

my $VIRT_SYS = "VM";


# Constructeur
sub NEW
{
    my ( $class, $host ) = @_;
    my $this = {};
    bless($this, $class);

    if( Vserver->is_enable() )
    {
	$this=Vserver->NEW();
    }
    elsif( OpenVZ->is_enable() )
    {
	$this=OpenVZ->NEW();
    }
    elsif( KVM->is_enable() )
    {
	$this=KVM->NEW();
    }
    elsif( Xen->is_enable() )
    {
	$this=Xen->NEW();
    }
    elsif( VMWare->is_enable() )
    {
	$this=VMWare->NEW();
    }
    else
    {
	if ($use_libvirt)
	{
	    $this=Libvirt->NEW($host);
	}
	else
	{
	    $this=0;
	}
    }
    $this->{host}=$host;
    return $this;
}

sub DESTROY
{
    my ( $this ) = @_;
}

sub get_virt_sys
{
    print STDOUT "Undefined get_virt_sys() fonction\n";
    exit 1;
}

sub is_enable
{
    print STDOUT "Undefined is_enable() fonction\n";
    exit 1;
}

sub start_vm
{
    print STDOUT "Undefined start_vm() fonction\n";
    exit 1;
}

sub stop_vm
{
    print STDOUT "Undefined stop_vm() fonction\n";
    exit 1;
}

sub stop_all_vm
{
    print STDOUT "Undefined stop_all_vm() fonction\n";
    exit 1;
}

sub enable_vm
{
    print STDOUT "Undefined enable_vm() fonction\n";
    exit 1;
}

sub disable_vm
{
    print STDOUT "Undefined disable_vm() fonction\n";
    exit 1;
}

sub get_vm_status
{
    print STDOUT "Undefined get_vm_status() fonction\n";
    exit 1;
}

sub exists_vm
{
    print STDOUT "Undefined exists_vm() fonction\n";
    exit 1;
}

sub get_all_vm
{
    print STDOUT "Undefined get_all_vm() fonction\n";
    exit 1;
}

sub get_started_vm
{
    print STDOUT "Undefined get_started_vm() fonction\n";
    exit 1;
}

sub get_vm_state
{
    print STDOUT "Undefined get_vm_state() fonction\n";
    exit 1;
}

sub get_header_stats
{
    print STDOUT "Undefined get_header_stats() fonction\n";
    exit 1;
}

sub get_vm_stats
{
    print STDOUT "Undefined get_vm_stats() fonction\n";
    exit 1;
}

1;
