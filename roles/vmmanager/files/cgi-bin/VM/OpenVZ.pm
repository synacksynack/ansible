#
# Module OpenVZ
#
package OpenVZ;

use strict;
use Script::Tools;
use VM::VM;
our @ISA = qw(VM);

my $VIRT_SYS = "OpenVZ";
my $EXEC_VM = "/usr/sbin/vzctl";
my $EXEC_VM_STAT = "/usr/sbin/vzlist";
my $TMP_STAT_FILE = "";

sub NEW
{
    my ( $class ) = @_;
    my $this = {};
    bless($this, $class);

    $TMP_STAT_FILE = `mktemp`;
    system( "sudo $EXEC_VM_STAT -a -o veid,numproc,numfile,cpuunits,kmemsize,laverage | column -t > $TMP_STAT_FILE" );

    return $this;
}

sub DESTROY
{
    my ( $this ) = @_;

    system( `rm -f $TMP_STAT_FILE` );
}

sub get_virt_sys
{
    my ( $this ) = @_;

    return $VIRT_SYS;
}

sub is_enable
{
    my ( $this ) = @_;
    my $active = 0;

    $active = `grep -c CONFIG_VE=y /boot/config-\`uname -r\` 2>/dev/null`;
    chomp( $active );

    return $active;
}

sub start_vm
{
    my ( $this, $vm ) = @_;
    exec_cmd( "sudo $EXEC_VM start $vm" );
}

sub stop_vm
{
    my ( $this, $vm ) = @_;
    exec_cmd( "sudo $EXEC_VM stop $vm" );
}

sub stop_all_vm
{
    my ( $this ) = @_;
    my @srv_list = $this->get_all_vm();

    foreach ( @srv_list )
    {
	$this->stop_vm( $_ );
    }
}

sub enable_vm
{
    my ( $this, $vm ) = @_;
    my $id = $this->get_vm_id( $vm );
    exec_cmd( "sudo $EXEC_VM set $id --onboot yes --save" );
}

sub disable_vm
{
    my ( $this, $vm ) = @_;
    my $id = $this->get_vm_id( $vm );
    exec_cmd( "sudo $EXEC_VM set $id --onboot no --save" );
}

sub get_vm_status
{
    my ( $this, $vm ) = @_;
    my $output = `sudo $EXEC_VM status $vm`;

    return $output;
}

sub exists_vm
{
    my ( $this, $vm ) = @_;
    my $id = $this->get_vm_id( $vm );

    if ( "<$id>" ne "<>" )
    {
	return 1;
    }
    else
    {
	return 0;
    }
}

sub get_vm_id
{
    my ( $this, $vm ) = @_;
    my $id = "";

    if ( "<$vm>" ne "<>" )
    {
	$id = `sudo $EXEC_VM_STAT -a -o veid -N $vm -H`;
	chomp( $id );
    }

    return $id;
}

sub get_all_vm
{
    my ( $this ) = @_;
    my @srv_list = split( /\s+/, `sudo $EXEC_VM_STAT -anH -o name` );

    return @srv_list;
}

sub get_started_vm
{
    my ( $this ) = @_;
    my @srv_list = split( /\s+/, `sudo $EXEC_VM_STAT -nH -o name` );

    return @srv_list;
}

sub get_vm_state
{
    my ( $this, $vm ) = @_;

    my $state = 0;
    my $cmd_output = `sudo $EXEC_VM status $vm | cut -d" " -f5`;

    chomp($cmd_output);

    if ( "<$cmd_output>" eq "<running>" )
    {
	$state = 1;
    }

    return $state;
}

sub get_header_stats
{
    my ( $this ) = @_;
    my $i = 0;
    my @header_stats = split( /\s+/, `head -1 $TMP_STAT_FILE` );
    chomp($header_stats[$i++]) foreach( @header_stats );

    return @header_stats;
}

sub get_vm_stats
{
    my ( $this, $vm ) = @_;
    my $i = 0;
    my $id = $this->get_vm_id( $vm );
    my @vm_stats = split( /\s+/, `grep $id $TMP_STAT_FILE` );
    chomp( $vm_stats[$i++] ) foreach( @vm_stats );

    return @vm_stats;
}

1;
