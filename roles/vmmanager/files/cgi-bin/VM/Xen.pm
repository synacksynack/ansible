#P
# Module Xen
#
package Xen;

use strict;
use File::Basename;
use Script::Tools;
use VM::VM;
our @ISA = qw(VM);

my $DIRNAME = dirname( $0 );
my $VIRT_SYS = "Xen";
my $EXEC_VM = "/usr/sbin/xl";
my $EXEC_VM_STAT = "/usr/sbin/xentop";
my $EXEC_ENABLE = "$DIRNAME/../scripts/xen_enable_vm.sh";
my $VM_CONF_PATH = "/etc/xen/conf";
my $VM_AUTO_PATH = "";
my $TMP_STAT_FILE = "";

sub NEW
{
    my ( $class ) = @_;
    my $this = {};
    my $res;
    bless($this, $class);

    $VM_AUTO_PATH = `grep ^XENDOMAINS_AUTO= /etc/default/xendomains | cut -d= -f2`;
    chomp( $VM_AUTO_PATH );
    $TMP_STAT_FILE = `mktemp`;
    system( "sudo $EXEC_VM_STAT -b -i 1	| column -t | grep -v \"Domain-0\" > $TMP_STAT_FILE" );

    return $this;
}

sub DESTROY
{
    my ( $this ) = @_;
    system( "rm -f $TMP_STAT_FILE" );
}

sub get_virt_sys
{
    my ( $this ) = @_;

    return $VIRT_SYS;
}

sub is_enable
{
    my ( $this ) = @_;
    my $active = 0;
    $active = `grep -c CONFIG_XEN=y /boot/config-\`uname -r\` 2>/dev/null`;
    chomp( $active );

    return $active;
}

sub start_vm
{
    my ( $this, $vm ) = @_;
    exec_cmd( "sudo $EXEC_VM create $VM_CONF_PATH/$vm" );
}

sub stop_vm
{
    my ( $this, $vm ) = @_;
    exec_cmd( "sudo $EXEC_VM shutdown $vm -w" );
}

sub stop_all_vm
{
    my ( $this ) = @_;
    my @srv_list = $this->get_all_vm();

    foreach ( @srv_list )
    {
	$this->stop_vm( $_ );
    }
}

sub enable_vm
{
    my ( $this, $vm ) = @_;
    exec_cmd( "sudo $EXEC_ENABLE $vm start" );
}

sub disable_vm
{
    my ( $this, $vm ) = @_;
    exec_cmd( "sudo $EXEC_ENABLE $vm stop" );
}

sub get_vm_status
{
    my ( $this, $vm ) = @_;
    my $vename = `grep '^[ 	]*name[ 	]*=' $vm | sed "s|[^']*'\([^']*\)'.*|\1|"`;
    my $output = `sudo $EXEC_VM list $vename 2>&1`;

    return $output;
}

sub exists_vm
{
    my ( $this, $vm ) = @_;
    if ( -f "$VM_CONF_PATH/$vm" )
    {
	return 1;
    }
    else
    {
	return 0;
    }
}

sub get_all_vm
{
    my ( $this ) = @_;
    my $i = 0;
    my @vm_list = split( /\s+/, `cd $VM_CONF_PATH && find * -maxdepth 0 -type f` );
    chomp($vm_list[$i++]) foreach( @vm_list );

    return @vm_list;
}

sub get_started_vm
{
    my ( $this ) = @_;
    my @start_vm;
    my @srv_list = $this->get_all_vm();

    foreach my $vm  ( @srv_list )
    {
	if ( $this->get_vm_state( $vm ) )
	{
	    push( @start_vm, $vm );
	}
    }

    return @start_vm;
}

sub get_vm_state
{
    my ( $this, $vm ) = @_;
    my $state = `grep -c ^$vm $TMP_STAT_FILE`;
    chomp( $state );

    return $state;
}

sub get_header_stats
{
    my ( $this ) = @_;
    my $i = 0;
    my @header_stats = split( /\s+/, `head -1 $TMP_STAT_FILE` );
    chomp($header_stats[$i++]) foreach( @header_stats );

    return @header_stats;
}

sub get_vm_stats
{
    my ( $this, $vm ) = @_;
    my $i = 0;
    my @vm_stats = split( /\s+/, `grep ^$vm $TMP_STAT_FILE` );
    chomp($vm_stats[$i++]) foreach( @vm_stats );

    return @vm_stats;
}

1;
