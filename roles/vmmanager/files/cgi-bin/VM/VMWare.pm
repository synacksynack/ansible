#
# Module VMWare
#
package VMWare;

use strict;
use Script::Tools;
use VM::VM;
our @ISA = qw(VM);

my $VIRT_SYS = "VMWare";
my $EXEC_VM = "/usr/bin/vmware-cmd";
my $EXEC_VM_STAT = "/usr/bin/vmware-cmd -l";
my $TMP_STAT_FILE = "";

sub NEW
{
    my ( $class ) = @_;
    my $this = {};
    bless($this, $class);

    return $this;
}

sub DESTROY
{
    my ( $this ) = @_;
}

sub is_enable
{
    my ( $this ) = @_;
    my $active = 0;

    $active = `grep -c CONFIG_VMNIX=y /boot/config-\`uname -r\` 2>/dev/null`;
    chomp( $active );

    return $active;
}

sub get_virt_sys
{
    my ( $this ) = @_;

    return $VIRT_SYS;
}

sub start_vm
{
    my ( $this, $vm ) = @_;
    my $id = $this->get_vm_id($vm);
    exec_cmd( "sudo vmware-cmd $id start" );
}

sub stop_vm
{
    my ( $this, $vm ) = @_;
    my $id = $this->get_vm_id($vm);
    exec_cmd( "sudo vmware-cmd $id stop" );
    sleep 10
}

sub stop_all_vm
{
    my ( $this ) = @_;
    my @srv_list = $this->get_all_vm();

    foreach ( @srv_list ) {
	    $this->stop_vm( $_ );
    }
}

sub enable_vm
{
    my ( $this, $vm ) = @_;
    #exec_cmd( "sudo $EXEC_SET_MARK $vm start" );

    my $id = $this->get_vm_id($vm);
    my @output = `grep $id /etc/vmware/hostd/vmInventory.xml --before 1 | grep objID | sed -e 's/<objID>//g' | sed -e 's/<\\\/objID>//g' | sed -e 's/ //g'`;
    chomp($output[0]);
    my $vmid=$output[0];

    system "sudo vimsh -e \"hostsvc/autostartmanager/update_autostartentry $vmid PowerOn 60 1 \\\"Shut down guest\\\" 1 systemDefault\" -n > /dev/null 2>&1";
}

sub disable_vm
{
    my ( $this, $vm ) = @_;
    my $id = $this->get_vm_id($vm);
    my @output = `grep $id /etc/vmware/hostd/vmInventory.xml --before 1 | grep objID | sed -e 's/<objID>//g' | sed -e 's/<\\\/objID>//g' | sed -e 's/ //g'`;
    chomp($output[0]);
    my $vmid=$output[0];
    system "sudo vimsh -e \"hostsvc/autostartmanager/update_autostartentry $vmid None 120 1 None 120 systemDefault\" -n > /dev/null 2>&1";
}

sub get_vm_status
{
    my ( $this, $vm ) = @_;
    my $id = $this->get_vm_id($vm);
    my @uptime = $this->get_vm_uptime($vm);
    if ( !defined($uptime[0])) { return "VM is starting" }
    else { return "VM is stoping"; }
}

sub exists_vm
{
    my ( $this, $vm ) = @_;
    if ( `sudo /usr/bin/vmware-cmd -l | egrep -o "$vm.vmx"` eq '')
    {
	return 0;
    }
    else
    {
	return 1;
    }
}

sub get_vm_id
{
    my ( $this, $vm ) = @_;
    my $id = `sudo vmware-cmd -l | grep $vm`;
    chomp( $id );

    return $id;
}

sub get_all_vm
{
    my ( $this ) = @_;
    my @srv_list = split( /\s+/,`sudo /usr/bin/vmware-cmd -l | egrep -o "[^/.]*\\.vmx" | egrep -o "[^.]*"`);

    return @srv_list;
}

sub get_started_vm
{
    my ( $this ) = @_;
    my @start_vm;
    my @srv_list = $this->get_all_vm();

    foreach my $vm ( @srv_list )
    {
	if ( $this->get_vm_state( $vm ) )
	{
	    push( @start_vm, $vm );
	}
    }

    return @start_vm;
}

sub get_vm_state
{
    my ( $this, $vm ) = @_;
    my $id = $this->get_vm_id($vm);
    my $state = `sudo vmware-cmd $id getstate | grep on > /dev/null && echo 1 || echo 0`;
    chomp( $state );

    return $state;
}

sub get_header_stats
{
    my @header_stats = ("Uptime(s)","");

    return @header_stats;
}

sub get_vm_uptime
{
    my ( $this, $vm ) = @_;
    my $id = $this->get_vm_id( $vm );

    my @uptime = (`sudo vmware-cmd $id getuptime | egrep -o "[0-9].*"`);

    return @uptime;
}

sub get_vm_stats
{
    my ( $this, $vm ) = @_;
    my @vm_stats = ($this->get_vm_uptime($vm),"");

    return @vm_stats;
}

1;
