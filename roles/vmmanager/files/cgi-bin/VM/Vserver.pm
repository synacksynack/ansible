#
# Module Vserver
#
package Vserver;

use strict;
use File::Basename;
use Script::Tools;
use VM::VM;
our @ISA = qw(VM);

my $DIRNAME = dirname( $0 );
my $VIRT_SYS = "Vserver";
my $EXEC_VM = "/usr/sbin/vserver";
my $EXEC_VM_STAT = "/usr/sbin/vserver-stat";
my $EXEC_ENABLE = "$DIRNAME/../scripts/vserver_enable_vm.sh";
my $TMP_STAT_FILE = "";

sub NEW
{
    my ( $class ) = @_;
    my $this = {};
    bless($this, $class);
    $TMP_STAT_FILE = `mktemp`;
    system( "sudo $EXEC_VM_STAT > $TMP_STAT_FILE") ;

    return $this;
}

sub DESTROY
{
    my ( $this ) = @_;
}

sub get_virt_sys
{
    my ( $this ) = @_;

    return $VIRT_SYS;
}

sub is_enable
{
    my ( $this ) = @_;
    my $active = 0;

    $active = `grep -c CONFIG_VSERVER=y /boot/config-\`uname -r\` 2>/dev/null`;
    chomp( $active );

    return $active;
}

sub start_vm
{
    my ( $this, $vm ) = @_;
    exec_cmd( "sudo $EXEC_VM $vm start" );
}

sub stop_vm
{
    my ( $this, $vm ) = @_;
    exec_cmd( "sudo $EXEC_VM $vm stop" );
}

sub stop_all_vm
{
    my ( $this ) = @_;
    my @srv_list = $this->get_all_vm();

    foreach ( @srv_list )
    {
	$this->stop_vm( $_ );
    }
}

sub enable_vm
{
    my ( $this, $vm ) = @_;
    exec_cmd( "sudo $EXEC_ENABLE $vm start" );
}

sub disable_vm
{
    my ( $this, $vm ) = @_;
    exec_cmd( "sudo $EXEC_ENABLE $vm stop" );
}

sub get_vm_status
{
    my ( $this, $vm ) = @_;
    my $output = `sudo $EXEC_VM $vm status`;

    return $output;
}

sub exists_vm
{
    my ( $this, $vm ) = @_;
    if ( -d "/etc/vservers/$vm" )
    {
	return 1;
    }
    else
    {
	return 0;
    }
}

sub get_vm_id
{
    my ( $this, $vm ) = @_;
    my $run_file = "/etc/vservers/$vm/run";
    my $id = `if [ -f $run_file ]; then cat $run_file; else echo -1; fi`;
    chomp( $id );

    return $id;
}

sub get_all_vm
{
    my ( $this ) = @_;
    my $i = 0;
    my @srv_list = split( /\s+/, `cd /etc/vservers/ && find * -maxdepth 0 -type d | grep -v MODELE` );

    return @srv_list;
}


sub get_started_vm
{
    my ( $this ) = @_;
    my @start_vm;
    my @srv_list = $this->get_all_vm();

    foreach my $vm ( @srv_list )
    {
	if ( $this->get_vm_state( $vm ) )
	{
	    push( @start_vm, $vm );
	}
    }

    return @start_vm;
}

sub get_vm_state
{
    my ( $this, $vm ) = @_;
    my $id = $this->get_vm_id( $vm );
    my $state = `grep -c ^$id $TMP_STAT_FILE`;
    chomp( $state );

    return $state;
}

sub get_header_stats
{
    my ( $this ) = @_;
    my $i = 0;
    my @header_stats = split( /\s+/, `head -1 $TMP_STAT_FILE` );
    chomp($header_stats[$i++]) foreach( @header_stats );

    return @header_stats;
}

sub get_vm_stats
{
    my ( $this, $vm ) = @_;
    my $i = 0;
    my $id = $this->get_vm_id( $vm );
    my @vm_stats = split( /\s+/, `grep ^$id $TMP_STAT_FILE` );
    chomp($vm_stats[$i++]) foreach( @vm_stats );

    return @vm_stats;
}

1;
