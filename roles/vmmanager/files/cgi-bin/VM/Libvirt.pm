#
# Module Libvirt
#
package Libvirt;

use strict;
use Script::Tools;
use VM::VM;
use Sys::Virt;
our @ISA = qw(VM);

my $VIRT_SYS = "Libvirt";
my $VIRT_CONF = "../scripts/libvirt_conf.txt";

sub _connect_libvirt
{
    my ($address, $username, $password) = @_;
    my $conn;

    eval
    {
       $conn = Sys::Virt->new(address  => $address,
			      auth     => 1,
			      credlist => [
					       Sys::Virt::CRED_AUTHNAME,
					       Sys::Virt::CRED_PASSPHRASE,
					  ],
			     callback  =>
		sub
		{
		    my $creds = shift;

		    foreach my $cred (@{$creds})
		    {
			if ($cred->{type} == Sys::Virt::CRED_AUTHNAME)
			{
			    $cred->{result} = $username;
			}
			if ($cred->{type} == Sys::Virt::CRED_PASSPHRASE)
			{
			    $cred->{result} = $password;
			}
		    }

		    return 0;
		});
    };

    if ($@)
    {
	print $@->message."<br/>";
	return 0;
    }
    else
    {
	return $conn;
    }
}

sub NEW
{
    my ( $class , $host) = @_;
    my $this = {};

    if (not $host)
    {
	print ("No host specified or invalid host<br>\n");
	return 0;
    }

    $this->{host} = $host;

    my $address;
    my $username;
    my $password;

    open CONFFILE, "<", $VIRT_CONF or print "Could not open $VIRT_CONF<br/>\n";
    my @line;
    while (<CONFFILE>)
    {
	chomp;
	@line = split /,/;
	if (@line == 4 and $line[0] eq $host)
	{
	    $address  = $line[1];
	    $username = $line[2];
	    $password = $line[3];
	}
    }
    if ($address)
    {
	my $conn = _connect_libvirt($address, $username, $password);
	if ($conn)
	{
	    $this->{_conn} = $conn;
	    bless($this, $class);
	    return $this;
	}
	else
	{
	    return 0;
	}
    }
    else
    {
	print "Could not find config for $host<br/>\n";
	return 0;
    }
}

sub DESTROY
{
    my ( $this ) = @_;
}

sub get_virt_sys
{
    my ( $this ) = @_;

    return $VIRT_SYS;
}

sub is_enable
{
    my ( $this ) = @_;
    my $active = 1;

    return $active;
}

sub start_vm
{
    my ( $this, $vm ) = @_;
    my $conn = $this->{_conn};
    my $dom = $conn->get_domain_by_name($vm);
    if ($dom)
    {
	eval
	{
	    $dom->create();
	};
	if ($@)
	{
	    print $@->message;
	}
    }
}

sub stop_vm
{
    my ( $this, $vm ) = @_;
    my $conn = $this->{_conn};
    my $dom = $conn->get_domain_by_name($vm);
    if ($dom)
    {
	eval
	{
	    $dom->shutdown();
	};
	if ($@)
	{
	    print $@->message;
	}
    }
}

sub stop_all_vm
{
    my ( $this ) = @_;
    my @srv_list = $this->get_all_vm();

    foreach ( @srv_list )
    {
	$this->stop_vm( $_ );
    }
}

sub enable_vm
{
    my ( $this, $vm ) = @_;
    my $conn = $this->{_conn};
    my $dom = $conn->get_domain_by_name($vm);
    if ($dom)
    {
	$dom->set_autostart(1);
    }
}

sub disable_vm
{
    my ( $this, $vm ) = @_;
    my $conn = $this->{_conn};
    my $dom = $conn->get_domain_by_name($vm);
    if ($dom)
    {
	$dom->set_autostart(0);
    }
}

sub get_vm_status
{
    my ( $this, $vm ) = @_;
    my $conn = $this->{_conn};
    my $dom = $conn->get_domain_by_name($vm);
    if ($dom)
    {
	my ($state, $reason) = $dom->get_state();
	return $reason;
    }
}

sub exists_vm
{
    my ( $this, $vm ) = @_;
    my $conn = $this->{_conn};
    my $dom = $conn->get_domain_by_name($vm);
    if ($dom)
    {
	return 1;
    }
    else
    {
	return 0;
    }
}

sub get_vm_id
{
    my ( $this, $vm ) = @_;
    my $conn = $this->{_conn};
    my $dom = $conn->get_domain_by_name($vm);
    if ($dom)
    {
	return $dom->get_id();
    }
}

sub get_all_vm
{
    my ( $this ) = @_;
    my $conn = $this->{_conn};
    my @dom_list = ( $conn->list_domains(), $conn->list_defined_domains());
    my @srv_list = map { $_->get_name() } @dom_list;

    return @srv_list;
}

sub get_started_vm
{
    my ( $this ) = @_;
    my $conn = $this->{_conn};
    my @dom_list = $conn->list_domains();
    my @srv_list = map { $_->get_name() } @dom_list;

    return @srv_list;
}

sub get_vm_state
{
    my ( $this, $vm ) = @_;
    my $conn = $this->{_conn};
    my $dom = $conn->get_domain_by_name($vm);
    if ($dom)
    {
	if ($dom->is_active())
	{
	    return 1;
	}
	else
	{
	    return 0;
	}
    }
}

sub get_header_stats
{
    my ( $this ) = @_;
    my @header_stats = ("maxMem","memory","cpuTime","nrVirtCpu","state","");
    return @header_stats;
}


sub get_vm_stats
{
    my ( $this, $vm ) = @_;
    my @vm_stats;
    my $conn = $this->{_conn};
    my $dom = $conn->get_domain_by_name($vm);
    if ($dom)
    {
	my $info = $dom->get_info();
	@vm_stats = ($info->{'maxMem'},
		     $info->{'memory'},
		     $info->{'cpuTime'},
		     $info->{'nrVirtCpu'},
		     $info->{'state'},
		     "");
	return (@vm_stats);
    }
}

1;
