#
# Module KVM
#
package KVM;

use strict;
use Script::Tools;
use VM::VM;
our @ISA = qw(VM);

my $VIRT_SYS = "KVM";
my $EXEC_VM = "/usr/bin/virsh --connect qemu:///system";
my $EXEC_VM_STAT = "/usr/local/bin/virshStat";
my $STOP_VM = "/usr/local/bin/qemu-shut";

# Fichier temporaire contenant la sortie des stats
my $TMP_STAT_FILE = "";

sub NEW
{
    my ( $class ) = @_;
    my $this = {};
    bless($this, $class);

    $TMP_STAT_FILE = `mktemp`;
    system( "sudo $EXEC_VM_STAT | column -t > $TMP_STAT_FILE" );

    return $this;
}

sub DESTROY
{
    my ( $this ) = @_;

    system( `rm -f $TMP_STAT_FILE` );
}

sub get_virt_sys
{
    my ( $this ) = @_;

    return $VIRT_SYS;
}

sub is_enable
{
    my ( $this ) = @_;
    my $active = 0;

    $active = `grep -c CONFIG_KVM=m /boot/config-\`uname -r\` 2>/dev/null`;
    chomp( $active );

    return $active;
}

sub start_vm
{
    my ( $this, $vm ) = @_;
    exec_cmd( "sudo $EXEC_VM start $vm" );
}

sub stop_vm
{
    my ( $this, $vm ) = @_;
    exec_cmd( "sudo $STOP_VM $vm" );
}

sub stop_all_vm
{
    my ( $this ) = @_;
    my @srv_list = $this->get_all_vm();

    foreach ( @srv_list )
    {
	$this->stop_vm( $_ );
    }
}

sub enable_vm
{
    my ( $this, $vm ) = @_;
    exec_cmd( "sudo $EXEC_VM autostart $vm" );
}

sub disable_vm
{
    my ( $this, $vm ) = @_;
    my $id = $this->get_vm_id( $vm );
    exec_cmd( "sudo $EXEC_VM autostart --disable $vm" );
}

sub get_vm_status
{
    my ( $this, $vm ) = @_;
    my $output = `sudo $EXEC_VM status $vm`;

    return $output;
}

sub exists_vm
{
    my ( $this, $vm ) = @_;
    my $id = $this->get_vm_id( $vm );

    if ( "<$id>" ne "<>" )
    {
	return 1;
    }
    else
    {
	return 0;
    }
}

sub get_vm_id
{
    my ( $this, $vm ) = @_;
    my $id = "";

    if ( "<$vm>" ne "<>" )
    {
	$id = `sudo $EXEC_VM domuuid $vm | tr -d '\n'`;
	chomp( $id );
    }
    return $id;
}

sub get_all_vm
{
    my ( $this ) = @_;
    my @srv_list = split( /\s+/, `sudo $EXEC_VM list --all | tr -s '[:blank:]' | sed '1,2d' | grep -v ^\$ | sed -e 's/^ //g' | cut -d" " -f2 | sort` );

    return @srv_list;
}

sub get_started_vm
{
    my ( $this ) = @_;
    my @srv_list = split( /\s+/, `sudo $EXEC_VM list | tr -s '[:blank:]' | sed '1,2d' | grep -v ^\$ | sed -e 's/^ //g' | cut -d" " -f2 | sort` );

    return @srv_list;
}

sub get_vm_state
{
    my ( $this, $vm ) = @_;
    my $result=`sudo $EXEC_VM domstate $vm | tr -d "\n"`;
    my $state = ( $result eq "running" ) ? 1 : 0;

    chomp( $state );

    return $state;
}

sub get_header_stats
{
    my ( $this ) = @_;
    my $i = 0;
    my @header_stats = split( /\s+/, `head -1 $TMP_STAT_FILE` );
    chomp($header_stats[$i++]) foreach( @header_stats );

    return @header_stats;
}

sub get_vm_stats
{
    my ( $this, $vm ) = @_;
    my $i = 0;
    my $id = $this->get_vm_id( $vm );
    my @vm_stats = split( /\s+/, `grep $id $TMP_STAT_FILE` );
    chomp( $vm_stats[$i++] ) foreach( @vm_stats );

    return @vm_stats;
}

1;
