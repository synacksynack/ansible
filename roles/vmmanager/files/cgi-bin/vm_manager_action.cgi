#!/usr/bin/perl -w

use strict;
use File::Glob ':glob';
use CGI(\*STDIN);
use Script::Config;
use Script::Tools;
use VM::VM;

my $cgi = new CGI;
my $START_VM = "start";
my $STOP_VM = "stop";
my $START_DAY = 0;
my $START_WEEK = 1;
my $DIRNAME="..";
my $START_VM_LIST="$DIRNAME/scripts/vm_start_list.txt";
my $HIDE_VM_LIST="$DIRNAME/scripts/vm_hide_list.txt";
my $server_name = $cgi->server_name();
my $user_name = $cgi->user_name();
my $remote_host = $cgi->remote_host();

main();

sub log_info
{
    my ( $msg ) = @_;
    my $date = `date "+%h %d %H:%M:%S"`;
    chomp( $date );

    open( FILE, ">>$LOG_FILE" ) or die "open $LOG_FILE: $!";
    print FILE "$date $server_name $user_name\@$remote_host: $msg\n";
    close( FILE );
}


sub exec_vm_action
{
    my ( $srv, $vm, $action, $week ) = @_;

    if ( $vm && ($action eq $START_VM || $action eq $STOP_VM) )
    {
	print $cgi->hr;

	if ( $action eq $START_VM )
	{
	    if ( "<$week>" eq "<$START_WEEK>" )
	    {
		print $cgi->h4( "Starting $vm for the week" );
		log_info( "Starting $vm for the week" );
		$srv->enable_vm( $vm );
	    }
	    else
	    {
		print $cgi->h4( "Starting $vm for the day" );
		log_info( "Starting $vm for the day" );
		$srv->disable_vm( $vm );
	    }
	    print "<pre>\n";
	    $srv->start_vm( $vm );
	    print "</pre>\n";

	    exec_cmd( "grep -q $vm $START_VM_LIST 2>/dev/null || echo $vm >>$START_VM_LIST" );
	}

	if ( $action eq $STOP_VM )
	{
		print $cgi->h4( "Stopping $vm" );
		log_info( "Stopping $vm" );
		$srv->disable_vm( $vm );

		print "<pre>\n";
		$srv->stop_vm( $vm );
		print "</pre>\n";
	}

	print "<br>";
	print $cgi->h4( "Status of $vm" );
	print "<pre>\n".$srv->get_vm_status( $vm )."</pre>\n";
    }
}

sub main
{
    my $srv;
    my $hide_vm;
    my $vm;

    print $cgi->header( -charset=>'utf-8' );
    print $cgi->start_html( -title=>"$server_name: VE Management", -style=>"css/global.css" );
    print $cgi->h2( $cgi->a( {-rel=>"Twiki", -href=>"$VMLIST_URL"}, "Server: $server_name" ) );

    my $host=$cgi->param('host');
    $host = "" if not $host =~ /^\w+$/;
    $srv = VM->NEW($host);

    if ( $srv )
    {
	$vm = $cgi->param('vm');
	$hide_vm = `grep -q ^$vm\$ $HIDE_VM_LIST 2>/dev/null && echo 1 || echo 0`;

	if ( $hide_vm != 1 )
	{
	    exec_vm_action( $srv, $cgi->param('vm'), $cgi->param('action'), $cgi->param('week') );
	}
	else
	{
	    print "$vm is protected and can't be started nor stopped through web UI";
	}

	print "<br><br>";
	print $cgi->a( {href=>"vm_manager.cgi?host=$host"},"Back to VE list of <b>$server_name</b>" );
    }
    else
    {
	print STDERR "Unable to establish which virtualization solution is installed";
	exit (1);
    }

    print $cgi->end_html;
}
