#!/usr/bin/perl -w

use strict;
use CGI(\*STDIN);

my $cgi = new CGI;
my $server_name = $cgi->server_name();
my $vm = $cgi->param('vm');
my $action = $cgi->param('action');
my $host = $cgi->param('host');
my @fqdn = split('\.', $server_name);
my $hook = "/$fqdn[0]";
$host = "" if not $host =~ /^\w+$/;

print $cgi->header( -charset=>'utf-8' );
print $cgi->start_html( -title=>"Confirm stopping $vm", -style=>"$hook/css/global.css" );

print $cgi->h3( "You are about to stop $vm. Are you sure?" );
print "<br>\n";

print "
<script language=\"javascript\">
    function stopAndClose()
    {
	window.opener.location = \"$hook/cgi-bin/vlist_manager_action.cgi?host=$host&vm=$vm&action=$action\";
	window.close();
    }
</script>";

print $cgi->a( {href=>"#", -onClick=>"window.close()"}, "<b>Back to VE list</b>" )."<br><br>\n";
print $cgi->a( {href=>"javascript:stopAndClose()"}, "<b>Yes, stop $vm</b>" );
print $cgi->end_html;
