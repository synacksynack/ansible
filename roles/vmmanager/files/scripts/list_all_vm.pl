#!/usr/bin/perl -w

use strict;
use File::Basename;
use Getopt::Long;

BEGIN
{
    my $DIRNAME = dirname( $0 );
    unshift( @INC, "${DIRNAME}/../cgi-bin/" );
}

use Script::Config;
use Script::Tools;
use VM::VM;

main();

sub syntax_usage
{
    print "$0 [-h|-a|-r|-s]\
	-h		displays this help message\
	-a		displays all VE\
	-r		displays running VE\
	-s		displays stopped VE\n";
}

sub display_vm_list
{
    my ( $ref_vm_list ) = @_;
    my @vm_list_sort = sort( @$ref_vm_list );

    foreach ( @vm_list_sort )
    {
	print "$_\n";
    }
}

sub main
{
    my $srv;
    my $help_opt = '';
    my $all_opt = '';
    my $run_opt = '';
    my $stop_opt = '';
    my @vm_list = ();
    my @res_list = ();

    GetOptions('h|help' => \$help_opt, 'a|all' => \$all_opt, 'r|run' => \$run_opt,
	       's|stop' => \$stop_opt );

    if ( $help_opt )
    {
	syntax_usage();
	exit( 0 );
    }

    $srv = VM->NEW();

    if ( $srv )
    {
	@vm_list = $srv->get_all_vm();

	if ( $run_opt )
	{
	    foreach ( @vm_list )
	    {
		push( @res_list, $_ ) if ( $srv->get_vm_state($_) );
	    }
	    display_vm_list( \@res_list );

	}
	elsif ( $stop_opt )
	{
	    foreach ( @vm_list )
	    {
		push( @res_list, $_ ) if ( ! $srv->get_vm_state($_) );
	    }
	    display_vm_list( \@res_list );

	}
	else
	{
	    display_vm_list( \@vm_list );
	}
    }
    else
    {
	print STDERR "Unable to establish which virtualization solution is installed";
	exit( 1 );
    }

    exit( 0 );
}
