#!/usr/bin/perl -w

use strict;
use File::Basename;
use Getopt::Long;
use POSIX qw(strftime);

BEGIN
{
    my $DIRNAME = dirname( $0 );
    unshift( @INC, "${DIRNAME}/../cgi-bin/" );
}

use Script::Config;
use Script::Tools;
use VM::VM;

my $INACT_SEC = $INACT_DAY * 86400;
my ( $FILENAME, $DIRNAME, $SUFFIX ) = fileparse( $0 );
my $ALL_VM_LIST = "$DIRNAME/all_vm_list.txt";
my $START_VM_LIST = "$DIRNAME/vm_start_list.txt";
my $STOP_VM_LIST = "$DIRNAME/vm_stop_list.txt";
my $INACT_VM_LIST = "$DIRNAME/vm_inact_list.txt";
my $WHITE_VM_LIST = "$DIRNAME/vm_whitelist.txt";
my $MAIL_FILE = "$DIRNAME/mail.txt";

chomp( $HOSTNAME );

main();

sub syntax_usage
{
    print STDERR "$0 [-f]\
	-h		prints help message\
	-f		stops virtual environments\n";
}

sub check_inact_vm
{
    my ( $srv ) = @_;
    my ( $current_date, $inact_date, $flag, $vm );
    my @content_file;

    my @all_vm = sort( $srv->get_all_vm() );


    open( FD, ">$ALL_VM_LIST" );
    foreach( @all_vm ) { print FD "$_\n"; }
    close( FD );

    system( "cat $START_VM_LIST | sort -u > ${START_VM_LIST}.tmp" );

    system( "comm -2 -3 $ALL_VM_LIST ${START_VM_LIST}.tmp > $STOP_VM_LIST" );

    system( "touch $INACT_VM_LIST" );

    $current_date = `date +%s`;
    chomp( $current_date );
    $inact_date = $current_date + $INACT_SEC;

    print STDOUT "Inactive VE of the week:\n";

    @content_file = ();
    get_content_file( $STOP_VM_LIST, \@content_file );
    foreach $vm ( @content_file )
    {
	system( "grep -q $vm $INACT_VM_LIST || echo \"$vm:$inact_date\" >>$INACT_VM_LIST" );
    }

    @content_file = ();
    get_content_file( "${START_VM_LIST}.tmp", \@content_file );
    foreach $vm ( @content_file )
    {
	system( "grep -q $vm $INACT_VM_LIST && sed -i \"/^$vm:/d\" $INACT_VM_LIST" );
    }

    $flag = 0;
    open( FD_MAIL, ">$MAIL_FILE" );
    print FD_MAIL "List of inactive VEs since at least $INACT_DAY days on $HOSTNAME:\n";

    @content_file = ();
    get_content_file( $INACT_VM_LIST, \@content_file );
    foreach my $entry ( @content_file )
    {
	$vm = `echo $entry | cut -d: -f1`;
	$inact_date = `echo $entry | cut -d: -f2`;

	chomp( $vm );
	chomp( $inact_date );

	if ( $inact_date < $current_date )
	{
	    if ( $srv->exists_vm( $vm ) )
	    {
		$inact_date = $inact_date - $INACT_SEC;
		my $format_inact_date = strftime "%Y-%m-%d %H:%M:%S", localtime($inact_date);
		chomp($format_inact_date);
		print FD_MAIL " - $vm  (inactive since $format_inact_date)\n";

		$flag = 1;
	    }
	    else
	    {
		system( "sed -i \"/^$vm:/d\" $INACT_VM_LIST" );
	    }
	}
    }

    close( FD_MAIL );

    if ( $flag == 1 )
    {
	send_mail( $MAIL_FILE, $MAIL_TO, "[REPORT] List of inactive VE on $HOSTNAME" );
    }

    system( "rm $MAIL_FILE ${START_VM_LIST}.tmp" );
}


sub stop_vm
{
    my ( $srv, $stop_flag ) = @_;
    my ( $vm, $date, $day, $month, $year, $limit_date, $white_vm_entry );
    my @vm_list = $srv->get_all_vm();
    my $flag = 0;
    my $current_date = `date +%s`;
    chomp( $current_date );

    open( FD_MAIL, ">$MAIL_FILE" );
    print FD_MAIL "List of VE removed from whitelist on $HOSTNAME:\n";

    system( "echo -n >$START_VM_LIST" ) if( $stop_flag );

    foreach $vm ( @vm_list )
    {
	if ( -f $WHITE_VM_LIST )
	{
	    $white_vm_entry = `grep "^$vm:" $WHITE_VM_LIST`;
	    chomp( $white_vm_entry );

	    if ( "<$white_vm_entry>" ne "<>" )
	    {
		$date = `echo $white_vm_entry | cut -d: -f2`; chomp( $date );
		$day = `echo $date | cut -d/ -f1`; chomp( $day );
		$month = `echo $date | cut -d/ -f2`; chomp( $month );
		$year = `echo $date | cut -d/ -f3`; chomp( $year );
		$limit_date = `date -d "$year-$month-$day" +%s`; chomp( $limit_date );

		if ( $current_date <= $limit_date && $srv->exists_vm( $vm ) )
		{
		    system( "echo $vm >>$START_VM_LIST" );
		    $srv->enable_vm( $vm );

		    if ( !$srv->get_vm_state($vm) )
		    {
			print STDOUT "Starting $vm from whitelist\n";
			$srv->start_vm( $vm );
		    }
		    next;
		}
		else
		{
		    print FD_MAIL " - $vm\n";
		    print STDOUT "Removing $vm from whiteList: expired";
		    system( "sed -i \"/^$vm:/d\" $WHITE_VM_LIST" );
		    $flag = 1;
		}
	    }
	}

	$srv->disable_vm( $vm ) if( $stop_flag );

	if ( $srv->get_vm_state($vm) && $stop_flag )
	{
	    print STDOUT "\n=> Stopping $vm <=\n";
	    $srv->stop_vm( $vm );
	}
    }

    if ( $flag == 1 )
    {
	send_mail( "$MAIL_FILE", "$MAIL_TO", "[REPORT] VE removed from whitelist on $HOSTNAME" );
    }

    system( "rm -f $MAIL_FILE" );
}

sub clear_white_list
{
    my ( $srv ) = @_;
    my $vm;
    my @content_file = ();

    get_content_file( $WHITE_VM_LIST, \@content_file );

    foreach my $entry (@content_file )
    {
	$vm = `echo $entry | cut -d: -f1`; chomp( $vm );

	if ( !$srv->exists_vm( $vm ) )
	{
	    print STDOUT " - $vm\n";
	    system( "sed -i \"/^$vm:/d\" $WHITE_VM_LIST" );
	}
    }
}

sub clear_inactive_list
{
    my ( $srv ) = @_;
    my $vm;
    my @content_file = ();

    get_content_file( $INACT_VM_LIST, \@content_file );

    foreach my $entry (@content_file )
    {
	$vm = `echo $entry | cut -d: -f1`; chomp( $vm );

	if ( !$srv->exists_vm( $vm ) )
	{
	    print STDOUT " - $vm\n";
	    system( "sed -i \"/^$vm:/d\" $INACT_VM_LIST" );
	}
    }
}

sub main
{
    my $srv;
    my $stop_flag = 0;
    my $help_opt = '';
    my $force_opt = '';

    GetOptions('h|help' => \$help_opt, 'f|force' => \$force_opt );

    if ($help_opt)
    {
	syntax_usage();
	exit( 0 );
    }

    if ($force_opt)
    {
	$stop_flag = 1;
    }

    $srv = VM->NEW();

    if ( $srv )
    {
	print STDOUT "\n=> Searching for inactive VE <=\n";
	check_inact_vm( $srv );

	print STDOUT "\n=> Stopping unmarked VE <=\n";
	stop_vm( $srv, $stop_flag );

	print STDOUT "\n=> Purging whitelist from migrated VE <=\n";
	clear_white_list( $srv );

	print STDOUT "\n=> Purging inactlist from migrated VE <=\n";
	clear_inactive_list( $srv );

	print STDOUT "\n=> Purging temp files <=\n";
	system( "rm -f $STOP_VM_LIST $ALL_VM_LIST" );
    }
    else
    {
	print STDERR "Unable to establish which virtualization solution is installed";
	exit (1);
    }

    exit( 0 );
}
