#!/usr/bin/perl

use strict;
use warnings;
use IO::Socket;
use Munin::Plugin;
use File::Basename;

if ( basename($0) !~ /(?:([^\/]+)_)?memcached_multi_/ ) {
    print
"This script needs to be named (prefix_)?memcached_multi_ to run properly.\n";
    exit 1;
}

need_multigraph();

my $host = $ENV{host} || "127.0.0.1";
my $port = $ENV{port} || 11211;
my $connection;
my $timescale = $ENV{timescale} || 3;
my $leitime = $ENV{leitime} || -1;
my $commands = $ENV{cmds} || "get set delete incr decr touch";
my %stats;
my %items;
my %chnks;
my $globalmap;
my %graphs;

$graphs{items} = {
    config => {
        args     => '--base 1000 --lower-limit 0',
        vlabel   => 'Items in Memcached',
        category => 'memcached global items',
        title    => 'Items',
        info     => 'Number of items in use by memcached',
    },
    datasrc => [
        { name => 'curr_items', label => 'Current Items', min => '0' },
        {
            name  => 'total_items',
            label => 'New Items',
            min   => '0',
            type  => 'DERIVE'
        },
    ],
};

$graphs{memory} = {
    config => {
        args     => '--base 1024 --lower-limit 0',
        vlabel   => 'Bytes Used',
        category => 'memcached global memory',
        title    => 'Memory Usage',
        info     => 'Memory consumption of memcached',
    },
    datasrc => [
        {
            name  => 'limit_maxbytes',
            draw  => 'AREA',
            label => 'Maximum Bytes Allocated',
            min   => '0'
        },
        {
            name  => 'bytes',
            draw  => 'AREA',
            label => 'Current Bytes Used',
            min   => '0'
        },
    ],
};

$graphs{bytes} = {
    config => {
        args     => '--base 1000',
        vlabel   => 'bits in (-) / out (+)',
        title    => 'Network Traffic',
        category => 'memcached',
        info     => 'Network traffic in (-) / out (+) of the machine',
        order    => 'bytes_read bytes_written',
    },
    datasrc => [
        {
            name  => 'bytes_read',
            type  => 'DERIVE',
            label => 'Network Traffic coming in (-)',
            graph => 'no',
            cdef  => 'bytes_read,8,*',
            min   => '0'
        },
        {
            name     => 'bytes_written',
            type     => 'DERIVE',
            label    => 'Traffic in (-) / out (+)',
            negative => 'bytes_read',
            cdef     => 'bytes_written,8,*',
            min      => '0'
        },
    ],
};

$graphs{conns} = {
    config => {
        args     => '--base 1000 --lower-limit 0',
        vlabel   => 'Connections per ${graph_period}',
        category => 'memcached',
        title    => 'Connections',
        info     => 'Number of connections being handled by memcached',
        order    => 'max_conns curr_conns avg_conns',
    },
    datasrc => [
        { name => 'curr_conns', label => 'Current Connections', min => '0' },
        { name => 'max_conns',  label => 'Max Connections',     min => '0' },
        { name => 'avg_conns',  label => 'Avg Connections',     min => '0' },
    ],
};

$graphs{commands} = {
    config => {
        args     => '--base 1000 --lower-limit 0',
        vlabel   => 'Commands per ${graph_period}',
        category => 'memcached global commands',
        title    => 'Commands',
        info     => 'Number of commands being handled by memcached',
    },
    datasrc => [
        {
            name  => 'cmd_get',
            type  => 'DERIVE',
            label => 'Gets',
            info  => 'Cumulative number of retrieval reqs',
            min   => '0'
        },
        {
            name  => 'cmd_set',
            type  => 'DERIVE',
            label => 'Sets',
            info  => 'Cumulative number of storage reqs',
            min   => '0'
        },
        {
            name  => 'cmd_flush',
            type  => 'DERIVE',
            label => 'Flushes',
            info  => 'Cumulative number of flush reqs',
            min   => '0'
        },
        {
            name  => 'cmd_touch',
            type  => 'DERIVE',
            label => 'Touches',
            info  => 'Cumulative number of touch reqs',
            min   => '0'
        },
        {
            name  => 'get_hits',
            type  => 'DERIVE',
            label => 'Get Hits',
            info  => 'Number of keys that were requested and found',
            min   => '0'
        },
        {
            name  => 'get_misses',
            type  => 'DERIVE',
            label => 'Get Misses',
            info  => 'Number of keys there were requested and not found',
            min   => '0'
        },
        {
            name  => 'delete_hits',
            type  => 'DERIVE',
            label => 'Delete Hits',
            info =>
              'Number of delete requests that resulted in a deletion of a key',
            min => '0'
        },
        {
            name  => 'delete_misses',
            type  => 'DERIVE',
            label => 'Delete Misses',
            info  => 'Number of delete requests for missing key',
            min   => '0'
        },
        {
            name  => 'incr_hits',
            type  => 'DERIVE',
            label => 'Increment Hits',
            info  => 'Number of successful increment requests',
            min   => '0'
        },
        {
            name  => 'incr_misses',
            type  => 'DERIVE',
            label => 'Increment Misses',
            info  => 'Number of unsuccessful increment requests',
            min   => '0'
        },
        {
            name  => 'decr_hits',
            type  => 'DERIVE',
            label => 'Decrement Hits',
            info  => 'Number of successful decrement requests',
            min   => '0'
        },
        {
            name  => 'decr_misses',
            type  => 'DERIVE',
            label => 'Decrement Misses',
            info  => 'Number of unsuccessful decrement requests',
            min   => '0'
        },
        {
            name  => 'cas_misses',
            type  => 'DERIVE',
            label => 'CAS Misses',
            info  => 'Number of Compare and Swap requests against missing keys',
            min   => '0'
        },
        {
            name  => 'cas_hits',
            type  => 'DERIVE',
            label => 'CAS Hits',
            info  => 'Number of successful Compare and Swap requests',
            min   => '0'
        },
        {
            name  => 'cas_badval',
            type  => 'DERIVE',
            label => 'CAS Badval',
            info  => 'Number of unsuccessful Compare and Swap requests',
            min   => '0'
        },
        {
            name  => 'touch_hits',
            type  => 'DERIVE',
            label => 'Touch Hits',
            info  => 'Number of successfully touched keys',
            min   => '0'
        },
        {
            name  => 'touch_misses',
            type  => 'DERIVE',
            label => 'Touch Misses',
            info  => 'Number of unsuccessful touch keys',
            min   => '0'
        },
    ],
};

$graphs{evictions} = {
    config => {
        args     => '--base 1000 --lower-limit 0',
        vlabel   => 'Evictions per ${graph_period}',
        category => 'memcached global evictions',
        title    => 'Evictions',
        info     => 'Number of evictions per second',
    },
    datasrc => [
        {
            name  => 'evictions',
            type  => 'DERIVE',
            label => 'Evictions',
            info  => 'Cumulative Evictions Across All Slabs',
            min   => '0'
        },
        {
            name  => 'evicted_nonzero',
            type  => 'DERIVE',
            label => 'Evictions prior to Expire',
            info => 'Cumulative Evictions forced to expire prior to expiration',
            min  => '0'
        },
        {
            name  => 'reclaimed',
            type  => 'DERIVE',
            label => 'Reclaimed Items',
            info  => 'Cumulative Reclaimed Item Entries Across All Slabs',
            min   => '0'
        },
    ],
};

$graphs{unfetched} = {
    config => {
        args     => '--base 1000 --lower-limit 0',
        vlabel   => 'Unfetched Items per ${graph_period}',
        category => 'memcached global unfetched',
        title    => 'Unfetched Items',
        info =>
'Number of items that were never touched get/incr/append/etc before X occured',
    },
    datasrc => [
        {
            name  => 'expired_unfetched',
            type  => 'DERIVE',
            label => 'Expired Unfetched',
            min   => '0',
            info =>
'Number of items that expired and never had get/incr/append/etc performed'
        },
        {
            name  => 'evicted_unfetched',
            type  => 'DERIVE',
            label => 'Evictioned Unfetched',
            min   => '0',
            info =>
'Number of items that evicted and never had get/incr/append/etc performed'
        },
    ],
};

$graphs{slabchnks} = {
    config => {
        args     => '--base 1000 --lower-limit 0',
        vlabel   => 'Available Chunks for this Slab',
        category => 'memcached slab chunk usage',
        title    => 'Chunk Usage for Slab: ',
        info => 'This graph shows you the chunk usage for this memory slab.',
    },
    datasrc => [
        {
            name  => 'total_chunks',
            label => 'Total Chunks Available',
            min   => '0'
        },
        { name => 'used_chunks', label => 'Total Chunks in Use', min => '0' },
        {
            name  => 'free_chunks',
            label => 'Total Chunks Not in Use (Free)',
            min   => '0'
        },
    ],
};

$graphs{slabhits} = {
    config => {
        args     => '--base 1000 --lower-limit 0',
        vlabel   => 'Hits per Slab per ${graph_period}',
        category => 'memcached slab commands',
        title    => 'Hits for Slab: ',
        info =>
          'This graph shows you the successful hit rate for this memory slab.',
    },
    datasrc => [
        {
            name  => 'get_hits',
            label => 'Get Requests',
            type  => 'DERIVE',
            min   => '0'
        },
        {
            name  => 'cmd_set',
            label => 'Set Requests',
            type  => 'DERIVE',
            min   => '0'
        },
        {
            name  => 'delete_hits',
            label => 'Delete Requests',
            type  => 'DERIVE',
            min   => '0'
        },
        {
            name  => 'incr_hits',
            label => 'Increment Requests',
            type  => 'DERIVE',
            min   => '0'
        },
        {
            name  => 'decr_hits',
            label => 'Decrement Requests',
            type  => 'DERIVE',
            min   => '0'
        },
        {
            name  => 'cas_hits',
            label => 'Sucessful CAS Requests',
            type  => 'DERIVE',
            min   => '0'
        },
        {
            name  => 'cas_badval',
            label => 'UnSucessful CAS Requests',
            type  => 'DERIVE',
            min   => '0'
        },
        {
            name  => 'touch_hits',
            label => 'Touch Requests',
            type  => 'DERIVE',
            min   => '0'
        },
    ],
};

$graphs{slabevics} = {
    config => {
        args     => '--base 1000 --lower-limit 0',
        vlabel   => 'Evictions per Slab per ${graph_period}',
        category => 'memcached slab evictions',
        title    => 'Evictions for Slab: ',
        info => 'This graph shows you the eviction rate for this memory slab.',
    },
    datasrc => [
        {
            name  => 'evicted',
            label => 'Total Evictions',
            type  => 'DERIVE',
            min   => '0',
            info  => 'Items evicted from memory slab'
        },
        {
            name  => 'evicted_nonzero',
            type  => 'DERIVE',
            label => 'Evictions from LRU Prior to Expire',
            info  => 'Items evicted from memory slab before ttl expiration',
            min   => '0'
        },
        {
            name  => 'reclaimed',
            type  => 'DERIVE',
            label => 'Reclaimed Expired Items',
            info =>
              'Number of times an item was stored in expired memory slab space',
            min => '0'
        },
    ],
};

$graphs{slabevictime} = {
    config => {
        args     => '--base 1000 --lower-limit 0',
        vlabel   => ' since Request for LEI',
        category => 'memcached slab eviction time',
        title    => 'Eviction Request Time for Slab: ',
        info =>
'This graph shows you the time since we requested the last evicted item',
    },
    datasrc => [
        {
            name  => 'evicted_time',
            label => 'Eviction Time (LEI)',
            info  => 'Time Since Request for Last Evicted Item',
            min   => '0'
        },
    ],
};

$graphs{slabitems} = {
    config => {
        args     => '--base 1000 --lower-limit 0',
        vlabel   => 'Items per Slab',
        category => 'memcached slab item count',
        title    => 'Items in Slab: ',
        info =>
'This graph shows you the number of items and reclaimed items per slab.',
    },
    datasrc => [
        {
            name  => 'number',
            label => 'Items',
            draw  => 'AREA',
            info  => 'This is the amount of items stored in this slab',
            min   => '0'
        },
    ],
};

$graphs{slabitemtime} = {
    config => {
        args     => '--base 1000 --lower-limit 0',
        vlabel   => ' since item was stored',
        category => 'memcached slab item age',
        title    => 'Age of Eldest Item in Slab: ',
        info => 'This graph shows you the time of the eldest item in this slab',
    },
    datasrc =>
      [ { name => 'age', label => 'Eldest Item\'s Age', min => '0' }, ],
};

$graphs{slabunfetched} = {
    config => {
        args     => '--base 1000 --lower-limit 0',
        vlabel   => 'Unfetched Items per ${graph_period}',
        category => 'memcached slab unfetched',
        title    => 'Unfetched Items in Slab: ',
        info =>
'Number of items that were never touched get/incr/append/etc before X occured',
    },
    datasrc => [
        {
            name  => 'expired_unfetched',
            type  => 'DERIVE',
            label => 'Expired Unfetched',
            min   => '0',
            info =>
'Number of items that expired and never had get/incr/append/etc performed'
        },
        {
            name  => 'evicted_unfetched',
            type  => 'DERIVE',
            label => 'Evictioned Unfetched',
            min   => '0',
            info =>
'Number of items that evicted and never had get/incr/append/etc performed'
        },
    ],
};

if ( defined $ARGV[0] && $ARGV[0] eq 'config' ) {
    $0 =~ /(?:([^\/]+)_)?memcached_multi_(.+)$/;
    my $prefix = $1 ? $1 : '';
    my $plugin = $2;
    die 'Unknown Plugin Specified: ' . ( $plugin ? $plugin : '' ) unless $graphs{$plugin};
    fetch_stats();
    $globalmap = buildglobalmap();
    do_config( $prefix, $plugin );
    exit 0;
}

if ( defined $ARGV[0] && $ARGV[0] eq 'autoconf' ) {
    my $s = get_conn();
    if ( defined($s) ) {
        print "yes\n";
        exit 0;
    }
    else {
        print "no (unable to connect to $connection)\n";
        exit 0;
    }
}

if ( defined $ARGV[0] && $ARGV[0] eq 'suggest' ) {
    my $s = get_conn();
    if ( defined($s) ) {
        fetch_stats();
        my @rootplugins =
          ( 'bytes', 'conns', 'commands', 'evictions', 'items', 'memory' );
        if ( $stats{version} !~ /^1\.4\.[0-7]$/ ) {
            push( @rootplugins, 'unfetched' );
        }
        foreach my $plugin (@rootplugins) {
            print "$plugin\n";
        }
        exit 0;
    }
    else {
        print "no (unable to connect to $connection)\n";
        exit 0;
    }
}

$0 =~ /(?:([^\/]+)_)?memcached_multi_(.+)$/;
my $prefix = $1 ? $1 : '';
my $plugin = $2;
die 'Unknown Plugin Specified: ' . ( $plugin ? $plugin : '' )
  unless $graphs{$plugin};
fetch_stats();
$globalmap = buildglobalmap();
fetch_output( $prefix, $plugin );

sub fetch_output {
    my ( $prefix, $plugin ) = (@_);
    my @subgraphs;
    if ( $plugin eq 'memory' ) {
        @subgraphs = ('slabchnks');
        foreach my $slabid ( sort { $a <=> $b } keys %chnks ) {
            print_submulti_output( $prefix, $slabid, $plugin, @subgraphs );
        }
        print_subrootmulti_output( $prefix, $plugin );
        print_rootmulti_output( $prefix, $plugin );
    }
    elsif ( $plugin eq 'commands' ) {
        @subgraphs = ('slabhits');
        foreach my $slabid ( sort { $a <=> $b } keys %chnks ) {
            print_submulti_output( $prefix, $slabid, $plugin, @subgraphs );
        }
        print_subrootmulti_output( $prefix, $plugin );
        print_rootmulti_output( $prefix, $plugin );
    }
    elsif ( $plugin eq 'evictions' ) {
        @subgraphs = ('slabevics');
        if ( $leitime == 1 ) { push( @subgraphs, 'slabevictime' ); }
        foreach my $slabid ( sort { $a <=> $b } keys %items ) {
            print_submulti_output( $prefix, $slabid, $plugin, @subgraphs );
        }
        print_subrootmulti_output( $prefix, $plugin );
        print_rootmulti_output( $prefix, $plugin );
    }
    elsif ( $plugin eq 'items' ) {
        @subgraphs = ( 'slabitems', 'slabitemtime' );
        foreach my $slabid ( sort { $a <=> $b } keys %items ) {
            print_submulti_output( $prefix, $slabid, $plugin, @subgraphs );
        }
        print_subrootmulti_output( $prefix, $plugin );
        print_rootmulti_output( $prefix, $plugin );
    }
    elsif ( $plugin eq 'unfetched' ) {
        @subgraphs = ('slabunfetched');
        foreach my $slabid ( sort { $a <=> $b } keys %items ) {
            print_submulti_output( $prefix, $slabid, $plugin, @subgraphs );
        }
        print_subrootmulti_output( $prefix, $plugin );
        print_rootmulti_output( $prefix, $plugin );
    }
    else {
        print_root_output($plugin);
    }

    return;
}

sub print_root_output {
    my ($plugin) = (@_);
    my $graph = $graphs{$plugin};
    if ( $plugin ne 'conns' ) {
        foreach my $dsrc ( @{ $graph->{datasrc} } ) {
            my %datasrc = %$dsrc;
            while ( my ( $key, $value ) = each(%datasrc) ) {
                next if ( $key ne 'name' );
                my $output = $stats{$value};
                print "$dsrc->{name}.value $output\n";
            }
        }
    }
    else {
        my $output;
        foreach my $dsrc ( @{ $graph->{datasrc} } ) {
            my %datasrc = %$dsrc;
            while ( my ( $key, $value ) = each(%datasrc) ) {
                if ( $value eq 'max_conns' ) {
                    $output = $stats{maxconns};
                }
                elsif ( $value eq 'curr_conns' ) {
                    $output = $stats{curr_connections};
                }
                elsif ( $value eq 'avg_conns' ) {
                    $output = sprintf( "%02d",
                        $stats{total_connections} / $stats{uptime} );
                }
                else {
                    next;
                }
                print "$dsrc->{name}.value $output\n";
            }
        }
    }
    return;
}

sub print_rootmulti_output {
    my ( $prefix, $plugin ) = (@_);
    my $graph = $graphs{$plugin};
    if ($prefix) {
        print "multigraph $prefix\_memcached_multi_$plugin\n";
    }
    else {
        print "multigraph memcached_multi_$plugin\n";
    }
    foreach my $dsrc ( @{ $graph->{datasrc} } ) {
        my $output  = 0;
        my %datasrc = %$dsrc;
        while ( my ( $key, $value ) = each(%datasrc) ) {
            next if ( $key ne 'name' );
            next
              if ( ( $plugin eq 'evictions' )
                && ( !exists( $globalmap->{globalevics}->{ $dsrc->{name} } ) )
              );
            next
              if ( ( $plugin eq 'commands' )
                && ( !exists( $globalmap->{globalcmds}->{ $dsrc->{name} } ) ) );
            if ( ( $plugin eq 'evictions' ) && ( $value eq 'evicted_nonzero' ) )
            {
                foreach my $slabid ( sort { $a <=> $b } keys %items ) {
                    $output += $items{$slabid}->{evicted_nonzero};
                }
            }
            else {
                $output = $stats{$value};
            }
            print "$dsrc->{name}.value $output\n";
        }
    }
    return;
}

sub print_subrootmulti_output {
    my ( $prefix, $plugin ) = (@_);
    my $graph = $graphs{$plugin};
    if ($prefix) {
        if ( $plugin eq 'evictions' ) {
            print "multigraph $prefix\_memcached_multi_$plugin.global$plugin\n";
        }
        else {
            print "multigraph $prefix\_memcached_multi_$plugin.$plugin\n";
        }
    }
    else {
        if ( $plugin eq 'evictions' ) {
            print "multigraph memcached_multi_$plugin.global$plugin\n";
        }
        else {
            print "multigraph memcached_multi_$plugin.$plugin\n";
        }
    }
    foreach my $dsrc ( @{ $graph->{datasrc} } ) {
        my $output  = 0;
        my %datasrc = %$dsrc;
        while ( my ( $key, $value ) = each(%datasrc) ) {
            next if ( $key ne 'name' );
            next
              if ( ( $plugin eq 'evictions' )
                && ( !exists( $globalmap->{globalevics}->{ $dsrc->{name} } ) )
              );
            next
              if ( ( $plugin eq 'commands' )
                && ( !exists( $globalmap->{globalcmds}->{ $dsrc->{name} } ) ) );
            if ( ( $plugin eq 'evictions' ) && ( $value eq 'evicted_nonzero' ) )
            {
                foreach my $slabid ( sort { $a <=> $b } keys %items ) {
                    $output += $items{$slabid}->{evicted_nonzero};
                }
            }
            else {
                $output = $stats{$value};
            }
            print "$dsrc->{name}.value $output\n";
        }
    }
    return;
}

sub print_submulti_output {
    my ( $prefix, $slabid, $plugin, @subgraphs ) = (@_);
    my $currslab = undef;
    foreach my $sgraph (@subgraphs) {
        my $graph = $graphs{$sgraph};
        if ($prefix) {
            print
              "multigraph $prefix\_memcached_multi_$plugin.$sgraph\_$slabid\n";
        }
        else {
            print "multigraph memcached_multi_$plugin.$sgraph\_$slabid\n";
        }
        if (   ( $plugin eq 'evictions' )
            || ( $plugin eq 'items' )
            || ( $plugin eq 'unfetched' ) )
        {
            $currslab = $items{$slabid};
        }
        elsif ( ( $plugin eq 'memory' ) || ( $plugin eq 'commands' ) ) {
            $currslab = $chnks{$slabid};
        }
        else {
            return;
        }
        foreach my $dsrc ( @{ $graph->{datasrc} } ) {
            my %datasrc = %$dsrc;
            while ( my ( $key, $value ) = each(%datasrc) ) {
                next if ( $key ne 'name' );
                next
                  if ( ( $sgraph eq 'slabevics' )
                    && ( !exists( $globalmap->{slabevics}->{ $dsrc->{name} } ) )
                  );
                next
                  if ( ( $plugin eq 'commands' )
                    && ( !exists( $globalmap->{slabcmds}->{ $dsrc->{name} } ) )
                  );
                my $output = $currslab->{$value};
                if (   ( $sgraph eq 'slabevictime' )
                    || ( $sgraph eq 'slabitemtime' ) )
                {
                    $output = time_scale( 'data', $output );
                }
                print "$dsrc->{name}.value $output\n";
            }
        }
    }
    return;
}

sub do_config {
    my ( $prefix, $plugin ) = (@_);
    my @subgraphs;
    if ( $plugin eq 'memory' ) {
        @subgraphs = ('slabchnks');
        foreach my $slabid ( sort { $a <=> $b } keys %chnks ) {
            print_submulti_config( $prefix, $slabid, $plugin, @subgraphs );
        }
        print_subrootmulti_config( $prefix, $plugin );
        print_rootmulti_config( $prefix, $plugin );
    }
    elsif ( $plugin eq 'commands' ) {
        @subgraphs = ('slabhits');
        foreach my $slabid ( sort { $a <=> $b } keys %chnks ) {
            print_submulti_config( $prefix, $slabid, $plugin, @subgraphs );
        }
        print_subrootmulti_config( $prefix, $plugin );
        print_rootmulti_config( $prefix, $plugin );
    }
    elsif ( $plugin eq 'evictions' ) {
        @subgraphs = ('slabevics');
        if ( $leitime == 1 ) { push( @subgraphs, 'slabevictime' ); }
        foreach my $slabid ( sort { $a <=> $b } keys %items ) {
            print_submulti_config( $prefix, $slabid, $plugin, @subgraphs );
        }
        print_subrootmulti_config( $prefix, $plugin );
        print_rootmulti_config( $prefix, $plugin );
    }
    elsif ( $plugin eq 'items' ) {
        @subgraphs = ( 'slabitems', 'slabitemtime' );
        foreach my $slabid ( sort { $a <=> $b } keys %items ) {
            print_submulti_config( $prefix, $slabid, $plugin, @subgraphs );
        }
        print_subrootmulti_config( $prefix, $plugin );
        print_rootmulti_config( $prefix, $plugin );
    }
    elsif ( $plugin eq 'unfetched' ) {
        @subgraphs = ('slabunfetched');
        foreach my $slabid ( sort { $a <=> $b } keys %items ) {
            print_submulti_config( $prefix, $slabid, $plugin, @subgraphs );
        }
        print_subrootmulti_config( $prefix, $plugin );
        print_rootmulti_config( $prefix, $plugin );
    }
    else {
        print_root_config( $prefix, $plugin );
    }

    return;
}

sub print_root_config {
    my ( $prefix, $plugin ) = (@_);
    my $graph     = $graphs{$plugin};
    my %graphconf = %{ $graph->{config} };
    while ( my ( $key, $value ) = each(%graphconf) ) {
        if ( $key eq 'title' ) {
            if ($prefix) {
                print "graph_$key " . ucfirst($prefix) . " $value\n";
            }
            else {
                print "graph_$key $value\n";
            }
        }
        else {
            print "graph_$key $value\n";
        }
    }
    foreach my $dsrc ( @{ $graph->{datasrc} } ) {
        my %datasrc = %$dsrc;
        while ( my ( $key, $value ) = each(%datasrc) ) {
            next if ( $key eq 'name' );
            print "$dsrc->{name}.$key $value\n";
        }
    }
    return;
}

sub print_rootmulti_config {
    my ( $prefix, $plugin ) = (@_);
    my $graph     = $graphs{$plugin};
    my %graphconf = %{ $graph->{config} };
    if ($prefix) {
        print "multigraph $prefix\_memcached_multi_$plugin\n";
    }
    else {
        print "multigraph memcached_multi_$plugin\n";
    }
    while ( my ( $key, $value ) = each(%graphconf) ) {
        if ( $key eq 'category' ) {
            print "graph_$key memcached\n";
        }
        elsif ( $key eq 'title' ) {
            if ($prefix) {
                print "graph_$key " . ucfirst($prefix) . " $value\n";
            }
            else {
                print "graph_$key $value\n";
            }
        }
        else {
            print "graph_$key $value\n";
        }
    }
    foreach my $dsrc ( @{ $graph->{datasrc} } ) {
        my %datasrc = %$dsrc;
        while ( my ( $key, $value ) = each(%datasrc) ) {
            next if ( $key eq 'name' );
            next
              if ( ( $plugin eq 'evictions' )
                && ( !exists( $globalmap->{globalevics}->{ $dsrc->{name} } ) )
              );
            next
              if ( ( $plugin eq 'commands' )
                && ( !exists( $globalmap->{globalcmds}->{ $dsrc->{name} } ) ) );
            print "$dsrc->{name}.$key $value\n";
        }
    }
    return;
}

sub print_subrootmulti_config {
    my ( $prefix, $plugin ) = (@_);
    my $graph     = $graphs{$plugin};
    my %graphconf = %{ $graph->{config} };
    if ($prefix) {
        if ( $plugin eq 'evictions' ) {
            print "multigraph $prefix\_memcached_multi_$plugin.global$plugin\n";
        }
        else {
            print "multigraph $prefix\_memcached_multi_$plugin.$plugin\n";
        }
    }
    else {
        if ( $plugin eq 'evictions' ) {
            print "multigraph memcached_multi_$plugin.global$plugin\n";
        }
        else {
            print "multigraph memcached_multi_$plugin.$plugin\n";
        }
    }

    while ( my ( $key, $value ) = each(%graphconf) ) {
        if ( $key eq 'title' ) {
            if ($prefix) {
                print "graph_$key " . ucfirst($prefix) . " $value\n";
            }
            else {
                print "graph_$key $value\n";
            }
        }
        else {
            print "graph_$key $value\n";
        }
    }
    foreach my $dsrc ( @{ $graph->{datasrc} } ) {
        my %datasrc = %$dsrc;
        while ( my ( $key, $value ) = each(%datasrc) ) {
            next if ( $key eq 'name' );
            next
              if ( ( $plugin eq 'evictions' )
                && ( !exists( $globalmap->{globalevics}->{ $dsrc->{name} } ) )
              );
            next
              if ( ( $plugin eq 'commands' )
                && ( !exists( $globalmap->{globalcmds}->{ $dsrc->{name} } ) ) );
            print "$dsrc->{name}.$key $value\n";
        }
    }
    return;
}

sub print_submulti_config {
    my ( $prefix, $slabid, $plugin, @subgraphs ) = (@_);
    my ( $slabitems, $slabchnks ) = undef;
    foreach my $sgraph (@subgraphs) {
        my $graph     = $graphs{$sgraph};
        my %graphconf = %{ $graph->{config} };
        if ($prefix) {
            print
              "multigraph $prefix\_memcached_multi_$plugin.$sgraph\_$slabid\n";
        }
        else {
            print "multigraph memcached_multi_$plugin.$sgraph\_$slabid\n";
        }
        while ( my ( $key, $value ) = each(%graphconf) ) {
            if ( $key eq 'title' ) {
                if ($prefix) {
                    print "graph_$key "
                      . ucfirst($prefix)
                      . " $value"
                      . "$slabid"
                      . " ($chnks{$slabid}->{chunk_size} Bytes)\n";
                }
                else {
                    print "graph_$key $value"
                      . "$slabid"
                      . " ($chnks{$slabid}->{chunk_size} Bytes)\n";
                }
            }
            elsif (
                ( $key eq 'vlabel' )
                && (   ( $sgraph eq 'slabevictime' )
                    || ( $sgraph eq 'slabitemtime' ) )
              )
            {
                $value = time_scale( 'config', $value );
                print "graph_$key $value\n";
            }
            else {
                print "graph_$key $value\n";
            }
        }
        foreach my $dsrc ( @{ $graph->{datasrc} } ) {
            my %datasrc = %$dsrc;
            while ( my ( $key, $value ) = each(%datasrc) ) {
                next if ( $key eq 'name' );
                next
                  if ( ( $sgraph eq 'slabevics' )
                    && ( !exists( $globalmap->{slabevics}->{ $dsrc->{name} } ) )
                  );
                next
                  if ( ( $plugin eq 'commands' )
                    && ( !exists( $globalmap->{slabcmds}->{ $dsrc->{name} } ) )
                  );
                print "$dsrc->{name}.$key $value\n";
            }
        }
    }
    return;
}

sub get_conn {
    my $s = undef;
    my ($sock) = ( $host =~ /unix:\/\/(.+)*$/ );

    if ($sock) {
        $connection = "unix:\/\/$sock";
        $s = IO::Socket::UNIX->new( Peer => $sock );
    }
    else {
        $connection = "$host:$port";
        $s          = IO::Socket::INET->new(
            Proto    => "tcp",
            PeerAddr => $host,
            PeerPort => $port,
            Timeout  => 10,
        );
    }
    return $s;
}

sub fetch_stats {
    my $s = get_conn();
    die "Error: Unable to Connect to $connection\n" unless $s;
    print $s "stats\r\n";
    while ( my $line = <$s> ) {
        if ( $line =~ /STAT\s(.+?)\s((\w|\d|\S)+)/ ) {
            my ( $skey, $svalue ) = ( $1, $2 );
            $stats{$skey} = $svalue;
        }
        last if $line =~ /^END/;
    }
    print $s "stats settings\r\n";
    while ( my $line = <$s> ) {
        if ( $line =~ /STAT\s(.+?)\s((\w|\d|\S)+)/ ) {
            my ( $skey, $svalue ) = ( $1, $2 );
            if ( $skey eq 'evictions' ) {
                $skey = 'evictions_active';
            }
            $stats{$skey} = $svalue;
        }
        last if $line =~ /^END/;
    }
    print $s "stats slabs\r\n";
    while ( my $line = <$s> ) {
        if ( $line =~ /STAT\s(\d+):(.+)\s(\d+)/ ) {
            my ( $slabid, $slabkey, $slabvalue ) = ( $1, $2, $3 );
            $chnks{$slabid}->{$slabkey} = $slabvalue;
        }
        last if $line =~ /^END/;
    }
    print $s "stats items\r\n";
    while ( my $line = <$s> ) {
        if ( $line =~ /STAT\sitems:(\d+):(.+?)\s(\d+)/ ) {
            my ( $itemid, $itemkey, $itemvalue ) = ( $1, $2, $3 );
            $items{$itemid}->{$itemkey} = $itemvalue;
        }
        last if $line =~ /^END/;
    }
}

sub time_scale {
    my ( $configopt, $origvalue ) = (@_);
    my $value;
    if ( $configopt eq 'config' ) {
        if ( $timescale == 1 ) {
            $value = "Seconds" . $origvalue;
        }
        elsif ( $timescale == 2 ) {
            $value = "Minutes" . $origvalue;
        }
        elsif (( $timescale == 3 )
            || ( $timescale > 4 )
            || ( !defined($timescale) ) )
        {
            $value = "Hours" . $origvalue;
        }
        elsif ( $timescale == 4 ) {
            $value = "Days" . $origvalue;
        }
    }
    elsif ( $configopt eq 'data' ) {
        if ( $timescale == 1 ) {
            $value = sprintf( "%02.2f", $origvalue / 1 );
        }
        elsif ( $timescale == 2 ) {
            $value = sprintf( "%02.2f", $origvalue / 60 );
        }
        elsif (( $timescale == 3 )
            || ( $timescale > 4 )
            || ( !defined($timescale) ) )
        {
            $value = sprintf( "%02.2f", $origvalue / 3600 );
        }
        elsif ( $timescale == 4 ) {
            $value = sprintf( "%02.2f", $origvalue / 86400 );
        }
    }
    else {
        die "Unknown time_scale option given: either [config/data]\n";
    }
    return $value;
}

sub buildglobalmap {
    my $results;
    my @cmds = split( ' ', $commands );
    foreach my $cmd (@cmds) {
        if ( $cmd eq "get" ) {
            $results->{globalcmds}->{cmd_get}    = 1;
            $results->{globalcmds}->{get_hits}   = 1;
            $results->{globalcmds}->{get_misses} = 1;
            $results->{slabcmds}->{get_hits}     = 1;
        }
        elsif ( $cmd eq "set" ) {
            $results->{globalcmds}->{cmd_set} = 1;
            $results->{slabcmds}->{cmd_set}   = 1;
        }
        elsif ( $cmd eq "delete" ) {
            $results->{globalcmds}->{delete_hits}   = 1;
            $results->{globalcmds}->{delete_misses} = 1;
            $results->{slabcmds}->{delete_hits}     = 1;
        }
        elsif ( $cmd eq "incr" ) {
            $results->{globalcmds}->{incr_hits}   = 1;
            $results->{globalcmds}->{incr_misses} = 1;
            $results->{slabcmds}->{incr_hits}     = 1;
        }
        elsif ( $cmd eq "decr" ) {
            $results->{globalcmds}->{decr_hits}   = 1;
            $results->{globalcmds}->{decr_misses} = 1;
            $results->{slabcmds}->{decr_hits}     = 1;
        }
        elsif ( $cmd eq "cas" ) {
            $results->{globalcmds}->{cas_hits}   = 1;
            $results->{globalcmds}->{cas_misses} = 1;
            $results->{globalcmds}->{cas_badval} = 1;
            $results->{slabcmds}->{cas_hits}     = 1;
            $results->{slabcmds}->{cas_badval}   = 1;
        }
        elsif ( $cmd eq "touch" ) {
            if ( $stats{version} !~ /^1\.4\.[0-7]$/ ) {
                $results->{globalcmds}->{cmd_touch}    = 1;
                $results->{globalcmds}->{touch_hits}   = 1;
                $results->{globalcmds}->{touch_misses} = 1;
                $results->{slabcmds}->{touch_hits}     = 1;
            }
        }
        elsif ( $cmd eq "flush" ) {
            if ( $stats{version} !~ /^1\.4\.[0-7]$/ ) {
                $results->{globalcmds}->{cmd_flush} = 1;
            }
        }
    }
    $results->{globalevics}->{evictions}       = 1;
    $results->{globalevics}->{evicted_nonzero} = 1;
    $results->{slabevics}->{evicted}           = 1;
    $results->{slabevics}->{evicted_nonzero}   = 1;
    if ( $stats{version} !~ /^1\.4\.[0-2]$/ ) {
        $results->{globalevics}->{reclaimed} = 1;
        $results->{slabevics}->{reclaimed}   = 1;
    }
    return $results;
}
